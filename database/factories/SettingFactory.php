<?php

use Faker\Generator as Faker;

$factory->define(App\Setting::class, function (Faker $faker) {
	return [
		'website_title' => 'ayfamu',
		'logo' => 'logo.png',
		'favicon' => 'favicon.png',
		'about_us' => 'Solusi Andalan Anak Tersayang.',
		'copyright' => 'Copyright 2019 <a href="#" target="_blank">ayfamu</a>, All rights reserved.',
		'email' => 'agung@ayfamu.com',
		'phone' => '+628998897943',
		'mobile' => '+628998897943',
		'fax' => '',
		'address_line_one' => 'Purwomartani, Kalasan, Sleman',
		'address_line_two' => 'Temanggal',
		'state' => 'Temanggal',
		'city' => 'Yogyakarta',
		'zip' => '12345',
		'country' => 'Indonesia',
		'map_iframe' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2642905.2881059386!2d89.27605108245604!3d23.817470325158617!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30adaaed80e18ba7%3A0xf2d28e0c4e1fc6b!2sBangladesh!5e0!3m2!1sen!2sbd!4v1520764767552" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
		'facebook' => 'https://facebook.com',
		'twitter' => 'https://twitter.com',
		'google_plus' => 'https://plus.google.com',
		'linkedin' => 'https://www.linkedin.com/',
		'meta_title' => 'ayfamu solusi anak tersayangmu',
		'meta_keywords' => 'anak, asi, booster, donasi, susu, dokter anak, dokter kandungan, rumah sakit, hamil, menyusui, melahirkan, lahiran, anak nangis, anak rewel, donor asi, kandungan',
		'meta_description' => 'AYFAMU hadir sebagai platform donasi online yang bertujuan untuk membantu moms diluar sana agar bisa saling berbagi dalam rangka pemenuhan kebutuhan Barang serta ASI Ekslusif untuk bayi mereka. Dengan AYFAMU juga moms bisa saling berbagi informasi terkait masalah pra kehamilan dan pasca kehamilan. AYFAMU juga akan mendata ANAK SESUSUAN bayi anda dari penerimaan donor ASI yang moms berikan.',
		'gallery_meta_title' => 'ayfamu solusi anak tersayangmu',
		'gallery_meta_keywords' => 'anak, asi, booster, donasi, susu, dokter anak, dokter kandungan, rumah sakit, hamil, menyusui, melahirkan, lahiran, anak nangis, anak rewel, donor asi, kandungan',
		'gallery_meta_description' => 'ayfamu solusi anak tersayangmu.',
	];
});
