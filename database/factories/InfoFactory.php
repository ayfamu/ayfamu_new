<?php

use Faker\Generator as Faker;

$factory->define(App\Info::class, function (Faker $faker) {
	$word = ucfirst($faker->word);
	$donasi_slug = $faker->word . '_' . $faker->word;
	$meta_keywords = $faker->word . ', ' . $faker->word . ', ' . $faker->word;

	return [
		'user_id' 				=> 2,
		'kategori_id' 			=> $faker->numberBetween($min = 1, $max = 2),
		'info_title' 			=> $faker->sentence,
		'info_details' 			=> $faker->text($minNbChars = 150, $maxNbChars = 1050),
		'info_date' 			=> $faker->date($format = 'Y-m-d', $max = 'now', $min = '2018-01-01'),
		'publication_status' 	=> $faker->numberBetween($min = 0, $max = 1),
		'meta_title' 			=> $word,
		'meta_keywords' 		=> $meta_keywords,
		'meta_description' 		=> $faker->text($maxNbChars = 150),
	];
});
