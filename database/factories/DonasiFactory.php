<?php

use Faker\Generator as Faker;

$factory->define(App\Donasi::class, function (Faker $faker) {
	$word = ucfirst($faker->word);
	$donasi_slug = $faker->word . '_' . $faker->word;
	$meta_keywords = $faker->word . ', ' . $faker->word . ', ' . $faker->word;

	return [
		'user_id' 				=> 2,
		'donasi_kategori_id' 	=> $faker->numberBetween($min = 2, $max = 3),
		'donasi_name' 			=> $faker->sentence,
		'donasi_slug' 			=> $donasi_slug,
		'donasi_details' 		=> $faker->text($minNbChars = 150, $maxNbChars = 1050),
		'donasi_stock' 			=> $faker->numberBetween($min = 1, $max = 10),
		'donasi_date' 			=> $faker->date($format = 'Y-m-d', $max = 'now', $min = '2018-01-01'),
		'publication_status' 	=> $faker->numberBetween($min = 0, $max = 1),
		'meta_title' 			=> $word,
		'meta_keywords' 		=> $meta_keywords,
		'meta_description' 		=> $faker->text($maxNbChars = 150),
	];
});
