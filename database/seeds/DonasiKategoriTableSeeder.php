<?php

use Illuminate\Database\Seeder;

class DonasiKategoriTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('donasi_kategori')->insert([
            [
                'id' => 1, 'parent_kategori_id' => NULL, 'donasi_kategori_name' => 'Donasi', 'donasi_kategori_desc' => 'Donasi',
            ],
            [
                'id' => 2, 'parent_kategori_id' => 1, 'donasi_kategori_name' => 'Donor ASI', 'donasi_kategori_desc' => 'Donor ASI',
            ],
            [
                'id' => 3, 'parent_kategori_id' => 1, 'donasi_kategori_name' => 'Donasi Barang', 'donasi_kategori_desc' => 'Donasi Barang',
            ],
        ]);
    }
}
