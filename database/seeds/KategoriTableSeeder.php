<?php

use Illuminate\Database\Seeder;

class KategoriTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('kategori')->insert([
            [
                'id' => 1, 'kategori_name' => 'Bayi', 'kategori_desc' => 'Info Bayi',
            ],
            [
                'id' => 2, 'kategori_name' => 'Rumah Sakit', 'kategori_desc' => 'Rumah Sakit',
            ],
        ]);
    }
}
