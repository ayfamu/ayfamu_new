<?php

use Illuminate\Database\Seeder;

class ObatTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('obat')->insert([
            [
                'id' => 1, 'obat_name' => 'ASI Booster',
            ],
        ]);
    }
}
