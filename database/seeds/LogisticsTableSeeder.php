<?php

use Illuminate\Database\Seeder;

class LogisticsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('logistics')->insert([
            [
                'id' 					=> 1, 
                'cover_shipping_fee' 	=> 0,
                'default_price' 		=> 0,
                'discount' 				=> 0,
                'display_name' 			=> 'COD Donatur',
                'enabled' 				=> 'true',
                'max_default_price' 	=> 0,
                'min_default_price' 	=> 0,
                'max_height' 			=> 40,
                'max_size' 				=> 7000,
                'name' 					=> 'COD Donatur',
                'preferred' 			=> 'false',
            ],
            [
                'id' 					=> 2, 
                'cover_shipping_fee' 	=> 0,
                'default_price' 		=> 14000,
                'discount' 				=> 0,
                'display_name' 			=> 'GrabExpress Sameday',
                'enabled' 				=> 'false',
                'max_default_price' 	=> 0,
                'min_default_price' 	=> 0,
                'max_height' 			=> 40,
                'max_size' 				=> 7000,
                'name' 					=> 'GrabExpress Sameday',
                'preferred' 			=> 'false',
            ],
            [
                'id' 					=> 3, 
                'cover_shipping_fee' 	=> 0,
                'default_price' 		=> 100,
                'discount' 				=> 0,
                'display_name' 			=> 'J&T Express',
                'enabled' 				=> 'false',
                'max_default_price' 	=> 0,
                'min_default_price' 	=> 0,
                'max_height' 			=> 0,
                'max_size' 				=> 50000,
                'name' 					=> 'J&T Express',
                'preferred' 			=> 'false',
            ],
        ]);
    }
}
