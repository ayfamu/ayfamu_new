<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
        $this->call(IndoRegionProvinceSeeder::class);
        $this->call(IndoRegionRegencySeeder::class);
        $this->call(IndoRegionDistrictSeeder::class);
        $this->call(IndoRegionVillageSeeder::class);
        
		$this->call(PenyakitTableSeeder::class);
		$this->call(ObatTableSeeder::class);
		$this->call(UsergroupsTableSeeder::class);
		$this->call(LogisticsTableSeeder::class);
		$this->call(UsersTableSeeder::class);
		$this->call(DonasiKategoriTableSeeder::class);
		// $this->call(DonasiTableSeeder::class);
		$this->call(SettingsTableSeeder::class);
		$this->call(KategoriTableSeeder::class);
		// $this->call(InfoTableSeeder::class);
	}
}
