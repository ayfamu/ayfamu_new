<?php

use Illuminate\Database\Seeder;

class PenyakitTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('penyakit')->insert([
            [
                'id' => 1, 'penyakit_name' => 'Diabetes',
            ],
            [
                'id' => 2, 'penyakit_name' => 'Jantung',
            ],
        ]);
    }
}
