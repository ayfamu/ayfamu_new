<?php

use Illuminate\Database\Seeder;

class UsergroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('usergroups')->insert([
            [
                'id' => 1, 'usergroup_name' => 'admin',
            ],
            [
                'id' => 2, 'usergroup_name' => 'user',
            ],
        ]);
    }
}
