<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
    	
    	DB::table('users')->insert([
            [
                'usergroup_id' => 1,
                'name' => 'admin',
    			'nik' => 'admin',
                'username' => 'admin',
                'email' => 'admin@mail.com',
                'password' => bcrypt('secret'),
    			'role' => 'admin',
    			'remember_token' => str_random(10),
                'tpt_lahir' => 34,
                'activation_status' => 1,
            ],
            [
                'usergroup_id' => 2,
                'name' => 'agung laksono hartadi',
                'nik' => '234564567646456',
                'username' => 'agung',
                'email' => 'agung.bg69@gmail.com',
                'password' => bcrypt('secret'),
                'role' => 'user',
                'remember_token' => str_random(10),
                'tpt_lahir' => 31,
                'activation_status' => 1,
            ],
            [
                'usergroup_id' => 2,
                'name' => $faker->name,
                'nik' => $faker->word . '' . $faker->word,
                'username' => $faker->word . '_' . $faker->word,
                'email' => $faker->unique()->safeEmail,
                'password' => bcrypt('secret'),
                'role' => 'user',
                'remember_token' => str_random(10),
                'tpt_lahir' => 34,
                'activation_status' => 1,
            ],
            [
                'usergroup_id' => 2,
                'name' => $faker->name,
                'nik' => $faker->word . '' . $faker->word,
                'username' => $faker->word . '_' . $faker->word,
                'email' => $faker->unique()->safeEmail,
                'password' => bcrypt('secret'),
                'role' => 'user',
                'remember_token' => str_random(10),
                'tpt_lahir' => 34,
                'activation_status' => 1,
            ],
            [
                'usergroup_id' => 2,
                'name' => $faker->name,
                'nik' => $faker->word . '' . $faker->word,
                'username' => $faker->word . '_' . $faker->word,
                'email' => $faker->unique()->safeEmail,
                'password' => bcrypt('secret'),
                'role' => 'user',
                'remember_token' => str_random(10),
                'tpt_lahir' => 34,
                'activation_status' => 1,
            ],
        ]);
    }
}
