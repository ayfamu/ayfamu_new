<?php

/*
 * This file is part of the IndoRegion package.
 *
 * (c) Azis Hapidin <azishapidin.com | azishapidin@gmail.com>
 *
 */

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistrictsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('indoregion_districts', function(Blueprint $table){
           // $table->char('id', 7);
           // $table->char('regency_id', 4);
           // $table->string('name', 50);
           
          $table->increments('id');
          $table->integer('regency_id')->unsigned()->index();

          $table->string('name', 50);
          
          $table->foreign('regency_id')->references('id')->on('indoregion_regencies')->onDelete('cascade');
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::drop('indoregion_districts');
     }
}
