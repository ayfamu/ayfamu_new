<?php

/*
 * This file is part of the IndoRegion package.
 *
 * (c) Azis Hapidin <azishapidin.com | azishapidin@gmail.com>
 *
 */

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVillagesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('indoregion_villages', function(Blueprint $table){
          // $table->char('id', 10);
          // $table->char('district_id', 7);
          // $table->string('name', 50);
           
          $table->bigIncrements('id');
          $table->integer('district_id')->unsigned()->index();

          $table->string('name', 50);
          
          $table->foreign('district_id')->references('id')->on('indoregion_districts')->onDelete('cascade');
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::drop('indoregion_villages');
     }
}
