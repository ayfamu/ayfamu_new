<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('users', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('usergroup_id')->unsigned()->index();
          	$table->integer('tpt_lahir')->nullable();
          	// $table->integer('tpt_lahir')->unsigned()->index();

			$table->string('name', 100);
			$table->string('nik', 100)->unique();
			$table->string('username', 100)->unique();
			$table->string('email', 100)->unique();
			$table->string('password');
			$table->string('avatar')->nullable();
			$table->string('gender')->nullable();
			$table->string('phone')->nullable();
			$table->string('address')->nullable();
            $table->date('tgl_lahir')->nullable();
            $table->string('agama')->nullable();
            $table->string('kode_pos')->nullable();

			$table->string('facebook')->nullable();
			$table->string('twitter')->nullable();
			$table->string('google_plus')->nullable();
			$table->string('linkedin')->nullable();
			$table->text('about')->nullable();
			$table->string('role', 50)->nullable();
			$table->tinyInteger('activation_status')->default(1);
			$table->tinyInteger('web_tour')->default(1);
			$table->rememberToken();
			$table->timestamps();

			$table->foreign('usergroup_id')->references('id')->on('usergroups')->onDelete('cascade');
          	// $table->foreign('tpt_lahir')->references('id')->on('indoregion_provinces')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('users');
	}
}
