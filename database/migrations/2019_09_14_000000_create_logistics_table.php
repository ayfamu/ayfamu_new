<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logistics', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('cover_shipping_fee');
            $table->decimal('default_price');
            $table->integer('discount');
            $table->string('display_name');
            $table->string('enabled');
            $table->decimal('max_default_price');
            $table->decimal('min_default_price');
            $table->integer('max_height');
            $table->integer('max_size');
            $table->string('name');
            $table->string('preferred');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logistics');
    }
}
