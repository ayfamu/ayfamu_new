<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBayiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bayi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();

            $table->string('bayi_avatar')->nullable();
            $table->string('bayi_name', 100);
            $table->string('bayi_nik', 100)->nullable();
            $table->string('bayi_gender')->nullable();
            $table->date('bayi_tgl_lahir')->nullable();
            $table->integer('bayi_tpt_lahir')->nullable();
            $table->tinyInteger('activation_status')->default(1);

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bayi');
    }
}
