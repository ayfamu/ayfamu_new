<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDonasiOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donasi_order', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->integer('pendonor_id')->unsigned()->index();
            $table->integer('logistic_id')->nullable()->unsigned()->index();
            $table->integer('user_alamat_id')->nullable()->unsigned()->index();

            $table->string('order_number')->nullable();
            $table->decimal('total_price')->nullable();

            $table->timestamps();
           
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('pendonor_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('logistic_id')->references('id')->on('logistics')->onDelete('cascade');
            $table->foreign('user_alamat_id')->references('id')->on('users_alamat')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donasi_order');
    }
}
