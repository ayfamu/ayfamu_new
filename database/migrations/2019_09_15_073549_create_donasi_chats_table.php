<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDonasiChatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donasi_chats', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('donasi_id')->unsigned()->index();
            $table->integer('send_by')->unsigned()->index();
            $table->integer('send_to')->unsigned()->index();

            $table->text('chat_message');
            $table->timestamp('chat_date');
            $table->timestamps();

            $table->foreign('donasi_id')->references('id')->on('donasi')->onDelete('cascade');
            $table->foreign('send_by')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('send_to')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donasi_chats');
    }
}
