<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDonasiBayiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donasi_bayi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('donasi_order_id')->unsigned()->index();
            $table->integer('donasi_id')->unsigned()->index();
            $table->integer('bayi_id')->unsigned()->index();

            $table->integer('req_stock')->default(0);
            $table->string('req_status');
            $table->text('req_details');
            $table->timestamp('req_date');

            $table->timestamps();

            $table->foreign('donasi_order_id')->references('id')->on('donasi_order')->onDelete('cascade');
            $table->foreign('donasi_id')->references('id')->on('donasi')->onDelete('cascade');
            $table->foreign('bayi_id')->references('id')->on('bayi')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donasi_bayi');
    }
}
