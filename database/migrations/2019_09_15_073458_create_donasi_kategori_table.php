<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDonasiKategoriTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donasi_kategori', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_kategori_id')->nullable();
            $table->string('donasi_kategori_name', 100);
            $table->text('donasi_kategori_desc')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donasi_kategori');
    }
}
