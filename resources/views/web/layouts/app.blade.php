<!DOCTYPE html>
<html lang="en">
<!-- head -->
<head>
    @include('web.includes.head')
    @yield('style')
</head>
<!-- /.head -->

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top" class="top">

    <!-- Preloader-->
    <!-- <div id="preloader">
      <div id="status"></div>
    </div> -->
    
    <!-- left section -->


    <span id="app">
      <index></index>

    </span>

    <!-- /. left section -->

    <!-- footer -->
    @include('web.includes.footer')
    <!-- /. footer -->

    <!-- /. theme-layout -->

    <!-- scripts -->
    @include('web.includes.scripts')
    @yield('script')
    <!-- /. script -->
</body>
</html>