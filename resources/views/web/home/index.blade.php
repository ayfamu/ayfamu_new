@extends('web.layouts.app')

@section('title', $setting->meta_title)
@section('keywords', $setting->meta_keywords)
@section('description', $setting->meta_description)

@section('style')
@endsection

@section('content')

<router-view name="dataWeb" :key="$route.fullPath"></router-view>

<!-- <router-view name="infoWeb" :key="$route.fullPath"></router-view> -->

@endsection

@section('script')
@endsection