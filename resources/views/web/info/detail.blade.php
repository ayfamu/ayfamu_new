@extends('web.layouts.app')

@section('title', $info->meta_title)
@section('keywords', $info->meta_keywords)
@section('description', $info->meta_description)

@section('style')
@endsection

@section('content')

<router-view name="infoWeb" :key="$route.fullPath"></router-view>

@endsection

@section('script')
@endsection