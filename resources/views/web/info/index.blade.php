@extends('web.layouts.app')

@section('title', 'Info Page')
@section('keywords', 'Informasi Anak')
@section('description', 'Informasi seputar anak dan bunda')

@section('style')
@endsection

@section('content')

<router-view name="infoWeb" :key="$route.fullPath"></router-view>

@endsection

@section('script')
@endsection