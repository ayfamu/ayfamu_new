<!-- Footer Section-->
<section class="footer bg-gray container-wave">

  <!-- <div class="banner-stars">
    <div class="stars"></div>
    <div class="twinkling"></div>
  </div> -->

  <!--Waves Container-->
  <!-- <svg class="waves" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28" preserveAspectRatio="none" shape-rendering="auto">
      <defs>
          <path id="gentle-wave" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z" />
      </defs>
      <g class="parallax">
          <use xlink:href="#gentle-wave" x="48" y="10" fill="rgba(0, 0, 0, 0.1)" />
          <use xlink:href="#gentle-wave" x="48" y="3" fill="rgba(230,230,230,0.5)" />
          <use xlink:href="#gentle-wave" x="48" y="5" fill="rgba(230,230,230,0.5)" />
          <use xlink:href="#gentle-wave" x="48" y="7" fill="rgba(255,255,255,0.5)" />
      </g>
  </svg> -->
    <!--Waves end-->
    
  <div class="box-wave">
    <div class='wave -one bg-dark'></div>
    <div class='wave -two bg-dark'></div>
    <div class='wave -three bg-white '></div>
  </div>

 <!--  <div class="banner-stars">
    <div class="stars"></div>
    <div class="twinkling"></div>
  </div> -->
  <div class="container">
    <div class="row hidden-xs">
      <div class="col-md-4 col-xs-4">
        <h3>Tentang Ayfamu</h3>
        <p>AYFAMU adalah aplikasi untuk membantu moms yang mengalami kekurangan ASI untuk mendapatkan ASI Eksklusif dan menemukan donasi barang pilihan moms.</p>
      </div>
      <div class="col-md-4 col-xs-4">
        <h3>Kenapa Ayfamu</h3>
        <p>Dengan AYFAMU dapat mempermudah moms dalam saling berbagi donasi barang-barang dan ASI yang dibutuhkan moms yang lain.</p>
      </div>
      <div class="col-md-3 col-xs-3">
        <h3>Contact</h3>
        <p><i class="fa fa-phone fa-fw"></i> (+628) 77-7416-4961 <br> <i class="fa fa-envelope fa-fw"></i> marketing@ayfamu.com <br> <i class="fa fa-map-marker fa-fw"></i> 55571 Kalasan, DI Yogyakarta
        </p>
      </div>
    </div>
    <hr class="hidden-xs">
    <div class="row">
      <div class="col-md-4 col-xs-4 hidden-xs">
        <ul class="list-inline">
          <li><a href="/"><i class="fa fa-twitter fa-fw fa-lg"></i></a></li>
          <li><a href="/"><i class="fa fa-facebook fa-fw fa-lg"></i></a></li>
          <li><a href="/"><i class="fa fa-google-plus fa-fw fa-lg"></i></a></li>
          <li><a href="/"><i class="fa fa-linkedin fa-fw fa-lg"></i></a></li>
        </ul>
      </div>
      <div class="col-md-8 col-xs-12">
        <p class="small">{!! $setting->copyright !!}</p>
      </div>
    </div>
  </div>
</section>