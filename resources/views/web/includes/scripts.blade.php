
<!-- jQuery-->
<script src="{{ asset('public/web/js/jquery-1.12.3.min.js') }}"></script>
<!-- Bootstrap Core JavaScript-->
<script src="{{ asset('public/web/js/bootstrap.min.js') }}"></script>
<!-- Plugin JavaScript-->
<script src="{{ asset('public/web/js/jquery.easing.min.js') }}"></script>
<script src="{{ asset('public/web/js/jquery.countdown.min.js') }}"></script>
<script src="{{ asset('public/web/js/device.min.js') }}"></script>
<script src="{{ asset('public/web/js/form.min.js') }}"></script>
<script src="{{ asset('public/web/js/jquery.placeholder.min.js') }}"></script>
<script src="{{ asset('public/web/js/jquery.shuffle.min.js') }}"></script>
<script src="{{ asset('public/web/js/jquery.parallax.min.js') }}"></script>
<script src="{{ asset('public/web/js/jquery.circle-progress.min.js') }}"></script>
<script src="{{ asset('public/web/js/jquery.swipebox.min.js') }}"></script>
<!-- <script src="{{ asset('public/web/js/smoothscroll.min.js') }}"></script> -->
<script src="{{ asset('public/web/js/tweecool.min.js') }}"></script>
<script src="{{ asset('public/web/js/wow.min.js') }}"></script>
<script src="{{ asset('public/web/js/jquery.smartmenus.js') }}"></script>
<!-- Custom Theme JavaScript-->
<script src="{{ asset('public/web/js/pheromone.js') }}"></script>
<script src="{{ asset('public/web/js/web.js') }}"></script>