<div class="collapse navbar-collapse navbar-main-collapse">
  <ul class="nav navbar-nav navbar-left">
    <li class="hidden"><a href="#page-top"></a></li>
    <li><a href="{{ route('homePage') }}">Home</a></li>
    <li><a href="{{ route('homePage', 'donasi') }}">Donasi</a></li>
    <li><a href="{{ route('homePage', 'info') }}">Info</a></li>
    <li><a href="{{ route('homePage', 'polling') }}">Polling</a></li>
    <li><a href="{{ route('homePage', 'silsilah') }}">Silsilah</a></li>
   <!--  <li><a href="#">Elements <i class="fa fa-angle-down"></i><span class="caret"></span></a>
      <ul class="dropdown-menu">
        <li><a href="buttons.html"><i class="fa fa-hand-pointer-o fa-lg fa-fw"></i> Buttons</a></li>
        <li><a href="carousels.html"><i class="fa fa-clone fa-lg fa-fw"></i> Carousels</a></li>
        <li><a href="columns.html"><i class="fa fa-th-list fa-lg fa-fw"></i> Columns <span class="label label-primary">update</span></a></li>
        <li><a href="pricing.html"><i class="fa fa-credit-card fa-lg fa-fw"></i> Pricing tables</a></li>
        <li><a href="countdown.html"><i class="fa fa-clock-o fa-lg fa-fw"></i> Countdown Timer</a></li>
        <li><a href="bars-charts.html"><i class="fa fa-pie-chart fa-lg fa-fw"></i> Bars and Charts</a></li>
        <li><a href="animate-on-scroll.html"><i class="fa fa-chevron-down fa-fw"></i> Animate On Scroll</a></li>
        <li><a href="typography.html"><i class="fa fa-text-width fa-lg fa-fw"></i> Typography</a></li>
        <li><a href="accordions.html"><i class="fa fa-plus-square-o fa-lg fa-fw"></i> Accordions</a></li>
      </ul>
    </li> -->
    <li class="menu-divider visible-lg">&nbsp;</li>
    <li class="dropdown"><a href="#" class="dropdown-toggle"><i class="fa fa-search fa-lg"></i><span class="caret"></span></a>
      <ul class="dropdown-menu">
        <li>
          <form method="post" class="search-form">
            <button type="submit" title="Search" class="search-button"><i class="fa fa-search fa-lg"></i></button>
            <input type="text" placeholder="SEARCH" class="form-control search-field">
          </form>
        </li>
      </ul>
    </li>
    <li class="dropdown"><a href="#" class="dropdown-toggle"><i class="fa fa-shopping-bag fa-lg"></i><span class="badge">3</span><span class="caret"></span></a>
      <ul class="dropdown-menu">
        <li class="shop-nav">
          <div class="shop-cart"><a href="shop-cart.html"><img src="{{ asset('public/web/img/shop/1.jpg') }}" alt=" ">
              <div class="btn-border btn btn-xs">$128.99</div><i class="fa fa-times float-right"></i><br>Jacket with contrasting hood</a></div>
        </li>
        <li class="shop-nav">
          <div class="shop-cart"><a href="shop-cart.html"><img src="{{ asset('public/web/img/shop/2.jpg') }}" alt=" ">
              <div class="btn-border btn btn-xs">$186.99</div><i class="fa fa-times float-right"></i><br>Jacket with a seam on the lapel</a></div>
        </li>
        <li class="shop-nav">
          <div class="shop-cart"><a href="shop-cart.html"><img src="{{ asset('public/web/img/shop/3.jpg') }}" alt=" ">
              <div class="btn-border btn btn-xs">$98.99</div><i class="fa fa-times float-right"></i><br>Faux leather jacket</a></div><span class="text-left">Subtotal:</span><span class="float-right"><b>$386.99</b></span>
        </li>
        <li><a href="shop-cart.html"><span class="btn btn-white btn-sm">Checkout</span></a></li>
      </ul>
    </li>

    <!-- <li><a href="#"><span class="lang">Register</span></a></li> -->

    <li class="dropdown"><a href="#" class="dropdown-toggle"><span class="lang">Profile</span><span class="caret"></span></a>
      <ul class="dropdown-menu">
        <li><a href="{{ route('homePage', 'akun') }}"><i class="fa fa-user"></i> Akun Saya</a></li>
        <li><a href="#"><i class="fa fa-sign-out"></i> Logout</a></li>
      </ul>
    </li>
<!--     <li class="dropdown"><a href="#" class="dropdown-toggle"><span class="lang">Eng</span><span class="caret"></span></a>
      <ul class="dropdown-menu">
        <li><a href="#">English</a></li>
        <li><a href="#">Español</a></li>
        <li><a href="#">Deutsch</a></li>
        <li><a href="#">Français</a></li>
        <li><a href="#">Русский</a></li>
        <li><a href="#">日本語</a></li>
        <li><a href="#">中文(简体)</a></li>
      </ul>
    </li> -->
  </ul>
</div>