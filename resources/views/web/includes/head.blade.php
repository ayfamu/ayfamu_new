<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon"  href="{{ asset('public/web/favicon/favicon.png') }}" />
<meta name="description" content="@yield('description')" />
<meta name="keywords" content="@yield('keywords')" />
<meta name="author" content="">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="theme-color" content="#933b81">
<title>@yield('title') - {{ $setting->website_title }}</title>
<!-- Bootstrap Core CSS-->
<link rel="stylesheet" href="{{ asset('public/web/css/bootstrap.min.css') }}">
<!-- Custom CSS-->
<link rel="stylesheet" href="{{ asset('public/web/css/pheromone.css') }}">
<link rel="stylesheet" href="{{ asset('public/web/css/custom.css') }}">
