<nav class="navbar navbar-custom navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" data-toggle="collapse" data-target=".navbar-main-collapse" class="navbar-toggle"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a href="#page-top" class="navbar-brand page-scroll">
        <!-- Img or text logo--><img src="{{ asset('public/web/img/logo.png') }}" alt="" class="logo"></a>
    </div>


    <!-- Menu Bar -->
    @include('web.includes.menubar')
    <!-- Menu Bar -->

  </div>
</nav>
