@extends('web.layouts.app')

@section('title', $donasi->meta_title)
@section('keywords', $donasi->meta_keywords)
@section('description', $donasi->meta_description)

@section('style')
@endsection

@section('content')

<router-view name="donasiWeb" :key="$route.fullPath"></router-view>

@endsection

@section('script')
@endsection