<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('public/web/favicon/favicon.png') }}">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>@yield('title') - {{ $setting->website_title }}</title>
    <!-- Bootstrap Core CSS-->
    <link href="{{ asset('public/web/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Custom CSS-->
    <link href="{{ asset('public/web/css/pheromone.css') }}" rel="stylesheet">
  </head>
  <body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top" class="top">
    <!-- Preloader-->
    <div id="preloader">
      <div id="status"></div>
    </div>
    <!-- Header-->
    <header data-background="{{ asset('public/web/img/header/0.jpg') }}" class="intro intro-fullscreen">
      <!-- Intro Header-->
      <div class="intro-body">
        <!-- Login-->
        <p><img src="{{ asset('public/web/img/logobig.png') }}" alt=""></p>
        <h4>Sign in</h4>
        <div class="container">
          <div class="row wow fadeIn">
            <div class="col-md-4 col-md-offset-4">
              <form class="form-signin" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}

                <label for="inputEmail" class="sr-only">Email address</label>
                <input id="inputEmail" type="text" placeholder="Email" required="" name="email" value="{{ old('email') }}" autofocus class="form-control input-lg">
                @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif

                <label for="inputPassword" class="sr-only">Password</label>
                <input id="inputPassword" type="password" placeholder="Password" required="" class="form-control input-lg" name="password">
                @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
                
                <div class="checkbox">
                  <label>
                    <input type="checkbox" value="remember-me"> Remember me
                  </label>
                </div>
                <button type="submit" class="btn btn-lg btn-dark btn-block">Sign in</button>
              </form>
            </div>
          </div>
          <!-- <div class="row">
            <div class="col-md-4 col-md-offset-4">
              <h5>Or login width</h5>
              <div class="row no-pad-top">
                <div class="col-sm-6">
                  <button class="btn btn-block btn-primary btn-xs"><i class="fa fa-facebook fa-2x"></i></button>
                </div>
                <div class="col-sm-6">
                  <button class="btn btn-block btn-info btn-xs"><i class="fa fa-twitter fa-2x"></i></button>
                </div>
              </div>
            </div>
          </div> -->
          <div class="row">
            <div class="col-md-6 col-md-offset-3"><i class="fa fa-map-marker"></i>  {{ $setting->address_line_one }}<br>
              {!! $setting->copyright !!}
            </div>
          </div>
        </div>
      </div>
    </header>


    <!-- jQuery-->
    <script type="text/javascript" src="{{ asset('public/web/js/jquery-1.12.3.min.js') }}"></script>
    <!-- Bootstrap Core JavaScript-->
    <script type="text/javascript" src="{{ asset('public/web/js/bootstrap.min.js') }}"></script>
    <!-- Plugin JavaScript-->

    <script src="{{ asset('public/web/js/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('public/web/js/jquery.countdown.min.js') }}"></script>
    <script src="{{ asset('public/web/js/device.min.js') }}"></script>
    <script src="{{ asset('public/web/js/form.min.js') }}"></script>
    <script src="{{ asset('public/web/js/jquery.placeholder.min.js') }}"></script>
    <script src="{{ asset('public/web/js/jquery.shuffle.min.js') }}"></script>
    <script src="{{ asset('public/web/js/jquery.parallax.min.js') }}"></script>
    <script src="{{ asset('public/web/js/jquery.circle-progress.min.js') }}"></script>
    <script src="{{ asset('public/web/js/jquery.swipebox.min.js') }}"></script>
    <script src="{{ asset('public/web/js/smoothscroll.min.js') }}"></script>
    <script src="{{ asset('public/web/js/tweecool.min.js') }}"></script>
    <script src="{{ asset('public/web/js/wow.min.js') }}"></script>
    <script src="{{ asset('public/web/js/pheromone.js') }}"></script>
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </body>
</html>