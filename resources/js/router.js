import VueRouter from 'vue-router'
// Pages
import About                  from './webcomponents/Home'
import Register               from './authcomponents/Register'
import Login                  from './authcomponents/Login'
import Dashboard              from './webcomponents/Dashboard'
import AdminDashboard         from './webcomponents/Dashboard'


import WebIndex         from './webcomponents/home/WebIndex.vue';
import SaranIndex       from './webcomponents/home/SaranIndex.vue';
// donasi route
import ItemIndex        from './webcomponents/item/ItemIndex.vue';
import ItemList         from './webcomponents/item/ItemList.vue';
import ItemDetail       from './webcomponents/item/ItemDetail.vue';

// checkout route
import CartIndex    from './webcomponents/item/CartIndex.vue';

// info route
import InfoIndex        from './webcomponents/info/InfoIndex.vue';
import InfoDetail       from './webcomponents/info/InfoDetail.vue';
// polling route
import PollingIndex     from './webcomponents/polling/PollingIndex.vue';
// silsilah route
import SilsilahIndex    from './webcomponents/silsilah/SilsilahIndex.vue';
// akun route
import AkunIndex        from './webcomponents/akun/AkunIndex.vue';
import AkunProfil       from './webcomponents/akun/AkunProfil.vue';
import AkunAnak         from './webcomponents/akun/AkunAnak.vue';
import AkunAlamat       from './webcomponents/akun/AkunAlamat.vue';
import AkunLogistic     from './webcomponents/akun/AkunLogistic.vue';
import AkunUbahpass     from './webcomponents/akun/AkunUbahpass.vue';
import AkunPesanan      from './webcomponents/akun/AkunPesanan.vue';
import AkunRequest      from './webcomponents/akun/AkunRequest.vue';
import AkunDonasi       from './webcomponents/akun/AkunDonasi.vue';
import AkunDonasidata   from './webcomponents/akun/AkunDonasidata.vue';
import AkunInfo         from './webcomponents/akun/AkunInfo.vue';
import AkunInfodata     from './webcomponents/akun/AkunInfodata.vue';
import AkunPolling      from './webcomponents/akun/AkunPolling.vue';
import AkunPollingdata  from './webcomponents/akun/AkunPollingdata.vue';
import AkunInbox        from './webcomponents/akun/AkunInbox.vue';
// Routes
const routes = [
  {
    path: '/akun',
    // name: 'akunWeb',
    components: {
        dataWeb: AkunIndex
    },
    meta: {
      auth: true
    },
    children:[
        {
          path: '',
          component: AkunProfil,
          props: { default: true, data: 'test data' },
          name: 'profil'
        },
        {
          path: '/data-anak',
          component: AkunAnak,
          props: true,
          name: 'dataAnak'
        },
        {
          path: '/alamat',
          component: AkunAlamat,
          props: true,
          name: 'alamat'
        },
        {
          path: '/jasa-kirim',
          component: AkunLogistic,
          props: true,
          name: 'logistic'
        },
        {
          path: '/ubah-password',
          component: AkunUbahpass,
          props: true,
          name: 'ubahPassword'
        },
        {
          path: '/pesanan-saya/:status?',
          component: AkunPesanan,
          props: true,
          name: 'pesananSaya'
        },
        {
          path: '/request-saya',
          component: AkunRequest,
          props: true,
          name: 'requestSaya'
        },
        {
          path: '/donasi-saya',
          component: AkunDonasi,
          props: true,
          name: 'donasiSaya'
        },
        {
          path: '/donasi-saya/data/:id?',
          component: AkunDonasidata,
          props: true,
          name: 'donasiData'
        },
        {
          path: '/info-saya',
          component: AkunInfo,
          props: true,
          name: 'infoSaya'
        },
        {
          path: '/info-saya/data/:id?',
          component: AkunInfodata,
          props: true,
          name: 'infoData'
        },
        {
          path: '/polling-saya',
          component: AkunPolling,
          props: true,
          name: 'pollingSaya'
        },
        {
          path: '/polling-saya/data/:id?',
          component: AkunPollingdata,
          props: true,
          name: 'pollingData'
        },
        {
          path: '/inbox-saya',
          component: AkunInbox,
          props: true,
          name: 'inboxSaya'
        }
    ]
  },
  {
    path: '/',
    name: 'homePage',
    components: {
        dataWeb: WebIndex
    },
    meta: {
      auth: undefined
    }
  },
  {
    path: '/kritik-saran',
    name: 'saranWeb',
    components: {
        dataWeb: SaranIndex
    },
    meta: {
      auth: undefined
    }
  },
  {
    path: '/donasi',
    name: 'itemWeb',
    components: {
        dataWeb: ItemIndex
    },
    meta: {
      auth: undefined
    }
  },
  {
    path: '/donasi/detail-item/:id', 
    name: 'itemDetail',
    components: {
        dataWeb: ItemDetail
    },
    meta: {
      auth: undefined
    }
  },
  {
    path: '/cart', 
    name: 'cartWeb',
    components: {
        dataWeb: CartIndex
    },
    meta: {
      auth: true
    }
  },
  {
    path: '/info',
    name: 'infoWeb',
    components: {
        dataWeb: InfoIndex
    },
    meta: {
      auth: undefined
    }
  },
  {
    path: '/info/detail-info/:id', 
    name: 'infoDetail',
    components: {
        dataWeb: InfoDetail
    },
    meta: {
      auth: undefined
    }
  },
  {
    path: '/polling',
    name: 'pollingWeb',
    components: {
        dataWeb: PollingIndex
    },
    meta: {
      auth: undefined
    }
  },
  {
    path: '/silsilah',
    name: 'silsilahWeb',
    components: {
        dataWeb: SilsilahIndex
    },
    meta: {
      auth: true
    }
  },
  {
    path: '/about',
    name: 'about',
    components: {
        dataWeb: About
    },
    meta: {
      auth: undefined
    }
  },
  {
    path: '/register',
    name: 'register',
    components: {
        dataWeb: Register
    },
    meta: {
      auth: false
    }
  },
  {
    path: '/login',
    name: 'login',
    components: {
        dataWeb: Login
    },
    meta: {
      auth: false
    }
  },
  // USER ROUTES
  {
    path: '/dashboard',
    name: 'dashboard',
    components: {
        dataWeb: Dashboard
    },
    meta: {
      auth: true
    }
  },
]
const router = new VueRouter({
  history: true,
  mode: 'history',
  routes,
})
export default router