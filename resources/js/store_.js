import Vue from 'vue'
import Vuex from 'vuex'
import auth from './auth.js'

Vue.use(Vuex)


const modules = {
    auth
}

const state = {
    token: localStorage.getItem('token'),
    errors: []
}
const getters = {
    //KITA MEMBUAT SEBUAH GETTERS DENGAN NAMA isAuth
    //DIMANA GETTERS INI AKAN BERNILAI TRUE/FALSE DENGAN KONDISI BERDASARKAN
    //STATE token.
    isAuth: state => {
        return state.token != "null" && state.token != null
    }
}
const mutations = {
    SET_TOKEN(state, payload) {
        state.token = payload
    },
    SET_ERRORS(state, payload) {
        state.errors = payload
    },
    CLEAR_ERRORS(state) {
        state.errors = []
    }
}
const actions = {
    
}
export default new Vuex.Store({
	modules,
    state,
    getters,
    mutations,
    actions
})