require('./global');
import 'vue-select/dist/vue-select.css';
import 'vue2-dropzone/dist/vue2Dropzone.min.css'
// Import stylesheet
import 'vue-loading-overlay/dist/vue-loading.css';

import 'vue-tour/dist/vue-tour.css';

// require('./bootstrap');
import 'es6-promise/auto'
import axios 			      from 'axios'
import Vue 				      from 'vue'
import VueAuth 			    from '@websanova/vue-auth'
import VueAxios 		    from 'vue-axios'
import VueRouter 		    from 'vue-router'
import VModal 			    from 'vue-js-modal'
import VueSimpleAlert 	from "vue-simple-alert";
import Index 			      from './webcomponents/layouts/App'
import auth 			      from './auth'
import router 			    from './router'
import Pagination       from 'laravel-vue-pagination';
import vSelect 			    from 'vue-select'
import vue2Dropzone     from 'vue2-dropzone'
import myUpload         from 'vue-image-crop-upload';
import Vue2Editor       from "vue2-editor";
import VueMask          from 'v-mask'
import Loading          from 'vue-loading-overlay';
import VueTour          from 'vue-tour'

// Set Vue globally
window.Vue = Vue
// Set Vue router
Vue.router = router
Vue.use(VueRouter)
// Set Vue authentication
Vue.use(VueAxios, axios)
axios.defaults.baseURL = `${process.env.MIX_APP_URL}/api`
Vue.use(VueAuth, auth)


// vue js guide tour
Vue.use(VueTour)

// Init plugin
Vue.use(Loading, {
  // props
  zIndex: 100001,
  color: '#ef5777',
  loader: 'dots',
  // is-full-page:true,
});

// masking form
Vue.use(VueMask);

// text editor
Vue.use(Vue2Editor);

// alert js
// type: 'success' | 'error' | 'warning' | 'info' | 'question'
Vue.use(VueSimpleAlert);
// modal vue js
// Vue.use(VModal, { dynamic: true, dynamicDefaults: { clickToClose: false }, injectModalsContainer: true})
Vue.use(VModal, { dynamic: true, injectModalsContainer: true})
// Vue.use(VModal, { componentName: "vue-modal",dynamic: true, injectModalsContainer: true })
// Vue.use(VModal, { componentName: "vue-modal" })

// Load Index
Vue.component('index', Index)
Vue.component('pagination', Pagination)
Vue.component('v-select', vSelect)
Vue.component('vueDropzone', vue2Dropzone)
Vue.component('my-upload', myUpload)

const app = new Vue({
  el: '#app',
  router
});