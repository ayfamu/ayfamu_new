
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./global');

// window.Vue = require('vue');

// import store from './store';
// import VueRouter from 'vue-router';
// window.Vue.use(VueRouter);

import Vue              from 'vue';
import VueRouter        from 'vue-router';
import axios            from 'axios';
import VueAxios         from 'vue-axios';


// import Register         from './components/Register.vue';
import Login            from './authcomponents/Login.vue';

import store            from './store';
 

Vue.use(VueRouter);
Vue.use(VueAxios, axios);


//vue-toaster
/*import Toaster          from 'v-toaster';
import 'v-toaster/dist/v-toaster.css';
 
Vue.use(Toaster, {timeout: 2000});
*/

const Default = { template: '<div class="default">default</div>' }

import Layout           from './webcomponents/layouts/App.vue';

import WebIndex         from './webcomponents/home/WebIndex.vue';
// donasi route
import ItemIndex        from './webcomponents/item/ItemIndex.vue';
import ItemList         from './webcomponents/item/ItemList.vue';
import ItemDetail       from './webcomponents/item/ItemDetail.vue';
// info route
import InfoIndex        from './webcomponents/info/InfoIndex.vue';
import InfoDetail       from './webcomponents/info/InfoDetail.vue';
// polling route
import PollingIndex     from './webcomponents/polling/PollingIndex.vue';
// silsilah route
import SilsilahIndex    from './webcomponents/silsilah/SilsilahIndex.vue';
// akun route
import AkunIndex        from './webcomponents/akun/AkunIndex.vue';
import AkunProfil       from './webcomponents/akun/AkunProfil.vue';
import AkunAnak         from './webcomponents/akun/AkunAnak.vue';
import AkunAlamat       from './webcomponents/akun/AkunAlamat.vue';
import AkunUbahpass     from './webcomponents/akun/AkunUbahpass.vue';
import AkunPesanan      from './webcomponents/akun/AkunPesanan.vue';
import AkunDonasi       from './webcomponents/akun/AkunDonasi.vue';
import AkunInfo         from './webcomponents/akun/AkunInfo.vue';
import AkunPolling      from './webcomponents/akun/AkunPolling.vue';
import AkunInbox        from './webcomponents/akun/AkunInbox.vue';


// import InfoList     from './webcomponents/info/InfoList.vue';

// Vue.component('pagination', require('laravel-vue-pagination'));
// Vue.component('polling-list', require('./webcomponents/polling/PollingList.vue'));

const routes = [
    {
        path: '/login',
        name: 'login',
        components: {
            dataWeb: Login
        },
        meta: {
            auth: false
        }
    },
    {
        path: '/akun',
        name: 'akunWeb',
        components: {
            dataWeb: AkunIndex
        },
        children:[
            {
              path: '',
              component: AkunProfil,
              name: 'profil'
            },
            {
              path: '/data-anak',
              component: AkunAnak,
              props: true,
              name: 'dataAnak'
            },
            {
              path: '/alamat',
              component: AkunAlamat,
              props: true,
              name: 'alamat'
            },
            {
              path: '/ubah-password',
              component: AkunUbahpass,
              props: true,
              name: 'ubahPassword'
            },
            {
              path: '/pesanan-saya/:status?',
              component: AkunPesanan,
              props: true,
              name: 'pesananSaya'
            },
            {
              path: '/donasi-saya',
              component: AkunDonasi,
              props: true,
              name: 'donasiSaya'
            },
            {
              path: '/info-saya',
              component: AkunInfo,
              props: true,
              name: 'infoSaya'
            },
            {
              path: '/polling-saya',
              component: AkunPolling,
              props: true,
              name: 'pollingSaya'
            },
            {
              path: '/inbox-saya',
              component: AkunInbox,
              props: true,
              name: 'inboxSaya'
            }
        ]
    },
    {
        path: '/',
        name: 'homePage',
        components: {
            dataWeb: WebIndex
        },
    },
    {
        path: '/donasi',
        name: 'itemWeb',
        components: {
            dataWeb: ItemIndex
        },
    },
    {
        path: '/detail-item/:id', 
        name: 'itemDetail',
        components: {
            dataWeb: ItemDetail
        },
    },
    {
        path: '/info',
        name: 'infoWeb',
        components: {
            dataWeb: InfoIndex
        },
    },
    {
        path: '/detail-info/:id', 
        name: 'infoDetail',
        components: {
            dataWeb: InfoDetail
        },
    },
    {
        path: '/polling',
        name: 'pollingWeb',
        components: {
            dataWeb: PollingIndex
        },
    },
    {
        path: '/silsilah',
        name: 'silsilahWeb',
        components: {
            dataWeb: SilsilahIndex
        },
    },
    // {path: '/admin/companies/create', component: ItemList, name: 'ItemList'},

]

 
const router = new VueRouter({ 
    mode:'history',
    routes 
})


//Navigation Guards
router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        let auth = store.getters.isAuth
        if (!auth) {
            next({ name: 'login' })
        } else {
            next()
        }
    } else {
        next()
    }
})
 
const app = new Vue({ 
	router ,
  store ,
  // component : {Layout}
  render: h => h(Layout)
}).$mount('#app')


/*new Vue({
    el: '#app',
    router,
    store,
    render: h => h(Layout)
    components: {
        Layout
    }
})*/