<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
	protected $table = 'kategori';

    protected $fillable = [
        'kategori_name', 'kategori_desc'
    ];

	public function polling() {
		return $this->hasMany(Polling::class);
	}

	public function info() {
		return $this->hasMany(Info::class);
	}
}
