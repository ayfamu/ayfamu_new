<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usergroup extends Model
{
    protected $fillable = [
        'usergroup_name'
    ];
    
	public function user() {
		return $this->hasMany(User::class);
	}
}
