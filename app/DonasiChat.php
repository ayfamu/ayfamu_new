<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DonasiChat extends Model
{
	protected $table = 'donasi_chats';

    protected $fillable = [
        'donasi_id', 'send_by', 'send_to', 'chat_message', 'chat_date'
    ];

	public function donasi() {
		return $this->belongsTo(Donasi::class);
	}
}
