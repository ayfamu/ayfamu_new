<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject{
	use Notifiable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'usergroup_id', 'tpt_lahir', 'name', 'nik', 'username', 'email', 'password', 'avatar', 'gender', 'phone', 'address', 'tgl_lahir', 'agama', 'kode_pos', 'facebook', 'twitter', 'google_plus', 'linkedin', 'about', 'role', 'activation_status','web_tour',

	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

 	public static function create(Request $request)
    {
        $user = new User();
        if (!empty($request->get('username'))) {
            $user->username = $request->get('username');
        }
        if (!empty($request->get('password'))) {
            $user->password = bcrypt($request->get('password'));
        }
        $user->save();
        return $user;
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    
    public function getJWTCustomClaims()
    {
        return [];
    }

	public function getAuthPassword()
	{
	    return $this->password;
	}

	public function usergroup() {
		return $this->belongsTo(Usergroup::class);
	}

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

	public function logistics() {
		return $this->belongsToMany(Logistic::class, 'user_logistic', 'user_id', 'logistic_id');
	}

	public function penyakits() {
		return $this->belongsToMany(Penyakit::class, 'riwayat_penyakit', 'user_id', 'penyakit_id');
	}

	public function obats() {
		return $this->belongsToMany(Obat::class, 'riwayat_obat', 'user_id', 'obat_id');
	}

	public function user_alamat() {
		return $this->hasMany(UserAlamat::class);
	}

	public function tag() {
		return $this->hasMany(Tag::class);
	}

	public function bayi() {
		return $this->hasMany(Bayi::class);
	}

	public function donasi_order() {
		return $this->hasMany(DonasiOrder::class);
	}

	public function donasi_order_pendonor() {
		return $this->hasMany(DonasiOrder::class, 'pendonor_id');
	}

	public function donasi() {
		return $this->hasMany(Donasi::class);
	}

	public function totalDonasi()
	{
	    return $this->donasi()
          	->leftJoin('donasi_kategori', 'donasi_kategori.id', '=', 'donasi.donasi_kategori_id')
	      	->selectRaw('sum(donasi.donasi_stock) as totalDonasi, donasi_kategori.donasi_kategori_name, donasi.donasi_kategori_id, donasi.user_id')
	      	->groupBy('donasi.donasi_kategori_id', 'donasi.user_id', 'donasi_kategori.donasi_kategori_name');
	}

	public function polling() {
		return $this->hasMany(Polling::class);
	}

	public function polling_user() {
		return $this->hasMany(PollingUser::class);
	}

	public function info() {
		return $this->hasMany(Info::class);
	}

	public function info_comment() {
		return $this->hasMany(InfoComment::class);
	}

	public function page() {
		return $this->hasMany(Page::class);
	}

	public function user_chat_by() {
		return $this->hasMany(UserChat::class, 'send_by');
	}

	public function user_chat_to() {
		return $this->hasMany(UserChat::class, 'send_to');
	}
}
