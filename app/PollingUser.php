<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PollingUser extends Model
{
	protected $table = 'polling_user';

    protected $fillable = [
        'user_id', 'polling_id', 'polling_count'
    ];

	public function user() {
		return $this->belongsTo(User::class);
	}

	public function polling() {
		return $this->belongsTo(Polling::class);
	}
}
