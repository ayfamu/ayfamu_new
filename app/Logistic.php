<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Logistic extends Model
{
    protected $fillable = [
        'cover_shipping_fee','default_price','discount','display_name','enabled','max_default_price','min_default_price','max_height','max_size','name','preferred'
    ];

	public function donasi_order() {
		return $this->hasMany(DonasiOrder::class);
	}

	public function users() {
		return $this->belongsToMany(User::class, 'user_logistic', 'logistic_id', 'user_id');
	}

}
