<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserChat extends Model
{
	protected $table = 'user_chat';

    protected $fillable = [
        'send_by', 'send_to', 'chat_message', 'chat_date'
    ];

	public function user_by() {
		return $this->belongsTo(User::class, 'send_by');
	}

	public function user_to() {
		return $this->belongsTo(User::class, 'send_to');
	}
}
