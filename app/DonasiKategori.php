<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DonasiKategori extends Model
{
	protected $table = 'donasi_kategori';

    protected $fillable = [
        'parent_kategori_id', 'donasi_kategori_name', 'donasi_kategori_desc'
    ];

	public function donasi() {
		return $this->hasMany(Donasi::class);
	}
}
