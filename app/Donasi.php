<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donasi extends Model
{
	protected $table = 'donasi';

    protected $fillable = [
        'user_id', 'donasi_kategori_id', 'donasi_name', 'donasi_slug', 'donasi_details', 'donasi_stock', 'donasi_date', 'publication_status', 'meta_title', 'meta_keywords', 'meta_description'
    ];

	public function user() {
		return $this->belongsTo(User::class);
	}

	public function tags() {
		return $this->belongsToMany(Tag::class, 'donasi_tag', 'donasi_id', 'tag_id');
	}

	public function donasi_kategori() {
		return $this->belongsTo(DonasiKategori::class);
	}

	public function donasi_chat() {
		return $this->hasMany(DonasiChat::class);
	}

	public function donasi_bayi() {
		return $this->hasMany(DonasiBayi::class);
	}

	public function donasi_gambar() {
		return $this->hasMany(DonasiGambar::class);
	}
}
