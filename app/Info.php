<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Info extends Model
{
	protected $table = 'info';

    protected $fillable = [
        'user_id', 'kategori_id', 'info_image', 'info_title', 'info_details', 'info_date', 'publication_status', 'meta_title', 'meta_keywords', 'meta_description'
    ];

	public function user() {
		return $this->belongsTo(User::class);
	}

	public function tags() {
		return $this->belongsToMany(Tag::class, 'info_tag', 'info_id', 'tag_id');
	}

	public function kategori() {
		return $this->belongsTo(Kategori::class);
	}

    public function info_comment() {
        return $this->hasMany(InfoComment::class);
    }
}
