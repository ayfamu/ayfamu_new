<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfoComment extends Model
{
	protected $table = 'info_comments';

    protected $fillable = [
        'user_id', 'info_id', 'parent_comment_id', 'comment', 'publication_status'
    ];

	public function user() {
		return $this->belongsTo(User::class);
	}

	public function info() {
		return $this->belongsTo(Info::class);
	}
}
