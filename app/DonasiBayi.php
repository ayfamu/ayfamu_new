<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DonasiBayi extends Model
{
	protected $table = 'donasi_bayi';

    protected $fillable = [
        'donasi_order_id', 'donasi_id', 'bayi_id', 'req_stock', 'req_status', 'req_details', 'req_date'
    ];

	public function bayi() {
		return $this->belongsTo(Bayi::class);
	}

	public function donasi_order() {
		return $this->belongsTo(DonasiOrder::class);
	}

	public function donasi() {
		return $this->belongsTo(Donasi::class);
	}
}
