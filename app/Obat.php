<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Obat extends Model
{
	protected $table = 'obat';

    protected $fillable = [
        'obat_name'
    ];

	public function users() {
		return $this->belongsToMany(User::class, 'riwayat_obat', 'obat_id', 'user_id');
	}
}
