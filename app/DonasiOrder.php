<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DonasiOrder extends Model
{
	protected $table = 'donasi_order';

    protected $fillable = [
        'user_id', 'pendonor_id', 'logistic_id','user_alamat_id', 'order_number','total_price'
    ];

	public function logistic() {
		return $this->belongsTo(Logistic::class);
	}

	public function user_alamat() {
		return $this->belongsTo(UserAlamat::class);
	}

	public function user() {
		return $this->belongsTo(User::class);
	}

	public function pendonor() {
		return $this->belongsTo(User::class, 'pendonor_id');
	}

	public function donasi_bayi() {
		return $this->hasMany(DonasiBayi::class);
	}
}
