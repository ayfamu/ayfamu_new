<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penyakit extends Model
{
	protected $table = 'penyakit';

    protected $fillable = [
        'penyakit_name'
    ];

	public function users() {
		return $this->belongsToMany(User::class, 'riwayat_penyakit', 'penyakit_id', 'user_id');
	}
}
