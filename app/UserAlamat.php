<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAlamat extends Model
{
	protected $table = 'users_alamat';

    protected $fillable = [
        'user_id', 'village_id', 'penerima', 'phone','kode_pos','alamat_lengkap','activation_status'
    ];

	public function user() {
		return $this->belongsTo(User::class);
	}

	public function village() {
		return $this->belongsTo(Village::class);
	}

	public function donasi_order() {
		return $this->hasMany(DonasiOrder::class);
	}
}
