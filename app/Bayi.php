<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bayi extends Model
{
	protected $table = 'bayi';

    protected $fillable = [
        'user_id', 'bayi_avatar', 'bayi_name', 'bayi_nik', 'bayi_gender', 'bayi_tgl_lahir','bayi_tpt_lahir'
    ];

	public function user() {
		return $this->belongsTo(User::class);
	}

	public function donasi_bayi() {
		return $this->hasMany(DonasiBayi::class);
	}
}
