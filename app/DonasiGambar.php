<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DonasiGambar extends Model
{
	protected $table = 'donasi_gambar';

    protected $fillable = [
        'donasi_id', 'donasi_image'
    ];

	public function donasi() {
		return $this->belongsTo(Donasi::class);
	}
}
