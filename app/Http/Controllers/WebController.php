<?php

namespace App\Http\Controllers;
use App\Setting;
use App\Donasi;
use App\Info;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Image;
use Purifier;

class WebController extends Controller {

	public function index($name=null, $id=null) 
	{
		$setting = Setting::first(['meta_title', 'meta_keywords', 'meta_description']);
		return view('web.home.index', compact('setting'));
	}

	public function info()
	{
		return view('web.info.index');
	}

	public function detail_info($id)
	{
        $info = Info::findOrFail($id);

		return view('web.info.detail', compact('info'));
	}

	public function detail_item($id)
	{
        $donasi = Donasi::findOrFail($id);

		return view('web.donasi.detail', compact('donasi'));
	}

}
