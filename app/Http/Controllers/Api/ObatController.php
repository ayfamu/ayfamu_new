<?php

namespace App\Http\Controllers\api;

use App\User;
use App\Obat;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class ObatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['obat'] = Obat::get();
        
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'obat_name'         => 'required|min:3|unique:obat',
        ], [
            'obat_name.unique'      => 'Nama obat sudah ada.',
            'obat_name.required'    => 'Nama obat harus diisi.',
        ]);

        if ($validator->passes()) {
            $obat = new Obat();
            $obat->obat_name        = $request->get('obat_name');
            
            $affected_row = $obat->save();

            if (!empty($affected_row)) {
                return Response::json(['status' => 'success', 'message' => 'Data obat berhasil disimpan.']);
            } else {
                return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
            }
        }
        return Response::json(['status' => 'errors', 'message' => $validator->errors()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $obat = Obat::find($id);
        return json_encode($obat);
    }

    public function obat_user($id)
    {
        $data['user'] = User::where('id', $id)->first();
        $data['obats'] = $data['user']->obats()->pluck('obat_id');

        return json_encode($data);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $obat = Obat::find($id);

        $validator = Validator::make($request->all(), [
            'obat_name'         => 'required|min:3',
        ], [
            'obat_name.required'  => 'Nama obat harus diisi.',
        ]);

        if ($validator->passes()) {
            $obat->obat_name        = $request->get('obat_name');
            
            $affected_row = $obat->save();

            if (!empty($affected_row)) {
                return Response::json(['status' => 'success', 'message' => 'Data obat berhasil diupdate.']);
            } else {
                return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
            }
        }
        return Response::json(['status' => 'errors', 'message' => $validator->errors()]);
    }

    public function update_obat(Request $request, $id)
    {
        $user = User::find($id);

        if (isset($request->obat_user)) {
            $user->obats()->sync($request->obat_user);
            return Response::json(['status' => 'success', 'message' => 'Data obat berhasil disimpan.']);
        } else {
            $user->obats()->sync(array());
            return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
