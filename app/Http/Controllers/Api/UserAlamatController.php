<?php

namespace App\Http\Controllers\api;

use App\UserAlamat;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class UserAlamatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'penerima'          => 'required|min:3',
            'phone'             => 'required|min:3',
            'kode_pos'          => 'required|min:5',
            'alamat_lengkap'    => 'required|min:5',
            'village_id'        => 'required',
        ], [
            'kode_pos.min'  => 'Minimal harus 5 angka.',
        ]);

        if ($validator->passes()) {
            $alamat = new UserAlamat();
            $alamat->user_id            = Auth::user()->id;
            $alamat->village_id         = $request->get('village_id');
            $alamat->penerima           = $request->get('penerima');
            $alamat->phone              = $request->get('phone');
            $alamat->kode_pos           = $request->get('kode_pos');
            $alamat->alamat_lengkap     = $request->get('alamat_lengkap');
            $alamat->activation_status  = $request->get('activation_status')?1:0;
            
            $affected_row = $alamat->save();

            if (!empty($affected_row)) {
                if($alamat->activation_status == 1){
                    UserAlamat::where('user_id',Auth::user()->id)
                                ->where('id','!=',$alamat->id)
                                ->update(['activation_status' => 0]);
                }

                return Response::json(['status' => 'success', 'message' => 'Data alamat berhasil disimpan.']);
            } else {
                return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
            }
        }
        return Response::json(['status' => 'errors', 'message' => $validator->errors()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // dd($id);
        $alamat = UserAlamat::where('users_alamat.id', $id)
                            ->leftJoin('indoregion_villages', 'indoregion_villages.id', '=', 'users_alamat.village_id')
                            ->leftJoin('indoregion_districts', 'indoregion_districts.id', '=', 'indoregion_villages.district_id')
                            ->leftJoin('indoregion_regencies', 'indoregion_regencies.id', '=', 'indoregion_districts.regency_id')
                            ->leftJoin('indoregion_provinces', 'indoregion_provinces.id', '=', 'indoregion_regencies.province_id')
                            ->select(array('users_alamat.*','indoregion_districts.id as district_id', 'indoregion_regencies.id as regency_id', 'indoregion_provinces.id as province_id'))
                            ->first();
        // dd($alamat);
        
        return json_encode($alamat);
    }

    public function alamat_user($id)
    {
        $data['alamat'] = UserAlamat::where('user_id', $id)
                                    ->leftJoin('indoregion_villages', 'indoregion_villages.id', '=', 'users_alamat.village_id')
                                    ->leftJoin('indoregion_districts', 'indoregion_districts.id', '=', 'indoregion_villages.district_id')
                                    ->leftJoin('indoregion_regencies', 'indoregion_regencies.id', '=', 'indoregion_districts.regency_id')
                                    ->leftJoin('indoregion_provinces', 'indoregion_provinces.id', '=', 'indoregion_regencies.province_id')
                                    ->select(array('users_alamat.*', 'indoregion_villages.name as kelurahan_name', 'indoregion_districts.name as kecamatan_name', 'indoregion_regencies.name as kabupaten_name', 'indoregion_provinces.name as propinsi_name'))
                                    ->get();

        return json_encode($data);
    }

    public function alamat_cek(){
        $data['alamat_active'] = UserAlamat::where('user_id', Auth::user()->id)->where('activation_status', 1)->count();

        return json_encode($data);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $alamat = UserAlamat::find($id);

        $validator = Validator::make($request->all(), [
            'penerima'          => 'required|min:3',
            'phone'             => 'required|min:3',
            'kode_pos'          => 'required|min:5',
            'alamat_lengkap'    => 'required|min:5',
            'village_id'        => 'required',
        ], [
            'kode_pos.min'  => 'Minimal harus 5 angka.',
        ]);

        if ($validator->passes()) {
            $alamat->village_id         = $request->get('village_id');
            $alamat->penerima           = $request->get('penerima');
            $alamat->phone              = $request->get('phone');
            $alamat->kode_pos           = $request->get('kode_pos');
            $alamat->alamat_lengkap     = $request->get('alamat_lengkap');
            $alamat->activation_status  = $request->get('activation_status')?1:0;
            
            $affected_row = $alamat->save();

            if (!empty($affected_row)) {
                if($alamat->activation_status == 1){
                    UserAlamat::where('user_id',Auth::user()->id)
                                ->where('id','!=',$alamat->id)
                                ->update(['activation_status' => 0]);
                }

                return Response::json(['status' => 'success', 'message' => 'Data alamat berhasil diupdate.']);
            } else {
                return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
            }
        }
        return Response::json(['status' => 'errors', 'message' => $validator->errors()]);
    }

    public function update_status(Request $request, $id){
        $alamat = UserAlamat::find($id);

        $alamat->activation_status  = $request->get('activation_status')?1:0;
        
        $affected_row = $alamat->save();

        if (!empty($affected_row)) {
            if($alamat->activation_status == 1){
                UserAlamat::where('user_id',Auth::user()->id)
                            ->where('id','!=',$alamat->id)
                            ->update(['activation_status' => 0]);
            }

            return Response::json(['status' => 'success', 'message' => 'Data alamat berhasil diupdate.']);
        } else {
            return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $alamat = UserAlamat::find($id);
        if ($alamat != null) {
            $alamat->delete();
            return Response::json(['status' => 'success', 'message' => 'Data alamat berhasil didelete.']);
        } else {
            return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
        }
    }
}
