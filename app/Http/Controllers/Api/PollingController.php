<?php

namespace App\Http\Controllers\api;

use App\Polling;
use App\PollingUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Image;
use Purifier;
use App\Http\Controllers\Controller;

class PollingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['polling'] = Polling::where('user_id', Auth::user()->id)->orderBy('polling_date', 'desc')->paginate(15);
        
        return $data;
    }

    public function group(Request $request, $name)
    {
        if($name == 'polling'){
            $data['polling'] = Polling::with('avgRating')
                                    ->where('polling.publication_status', 1)
                                    ->leftJoin('users', 'users.id', '=', 'polling.user_id')
                                    ->select(array('polling.*','users.name as user_name','users.avatar'))
                                    ->orderBy('polling.polling_date', 'desc')
                                    ->paginate(12);
        }else if($name == 'home'){
            // $data['donasi'] = Donasi::paginate(8);
            $data['polling']['data'] = Polling::where('publication_status', 1)
                                    ->orderBy('polling_date', 'desc')
                                    ->limit(6)
                                    ->get();
        }else{
            // $data['donasi'] = Donasi::paginate(8);
            $data['polling']['data'] = Polling::where('publication_status', 1)
                                    ->orderBy('polling_date', 'desc')
                                    ->limit(3)->get();
        }
        
        return $data;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'kategori_id'   => 'required',
            'polling_title'    => 'required|min:3',
            'polling_details'  => 'required|min:3',
        ]);

        if ($validator->passes()) {
            $polling = new Polling();
            $polling->user_id              = Auth::user()->id;
            $polling->kategori_id          = $request->get('kategori_id');
            // $polling->polling_image           = $request->get('polling_image');
            $polling->polling_title           = $request->get('polling_title');
            $polling->polling_details         = $request->get('polling_details');
            $polling->meta_title           = $request->get('meta_title');
            $polling->meta_description     = $request->get('meta_description');
            
            $affected_row = $polling->save();

            if (!empty($affected_row)) {
                return Response::json(['status' => 'success', 'message' => 'Data polling berhasil disimpan.', 'polling_id' => $polling->id]);
            } else {
                return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
            }
        }
        return Response::json(['status' => 'errors', 'message' => $validator->errors()]);
    }

    public function polling_user(Request $request){
        $polling_user = PollingUser::where('user_id', Auth::user()->id)
                                            ->where('polling_id', $request->get('polling_id'))
                                            ->first();
        if($polling_user){
            $polling_user->polling_count = $request->get('polling_count');

            $affected_row = $polling_user->save();
        }
        else{
            $polling_user = new PollingUser();
            $polling_user->user_id          = Auth::user()->id;
            $polling_user->polling_id       = $request->get('polling_id');
            $polling_user->polling_count    = $request->get('polling_count');
            
            $affected_row = $polling_user->save();
        }


        if (!empty($affected_row)) {
            return Response::json(['status' => 'success', 'message' => 'Anda berhasil melakukan polling.']);
        } else {
            return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['polling'] = Polling::findOrFail($id);

        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function show_edit_polling($id)
    {
        $data['polling'] = Polling::where('user_id', Auth::user()->id)->find($id);
        return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $polling = Polling::find($id);
        $validator = Validator::make($request->all(), [
            'kategori_id'   => 'required',
            'polling_title'    => 'required|min:3',
            'polling_details'  => 'required|min:3',
        ]);

        if ($validator->passes() && $polling->user_id == Auth::user()->id) {
            $polling->kategori_id          = $request->get('kategori_id');
            $polling->polling_image           = $request->get('polling_image').'?t='.rand();
            $polling->polling_title           = $request->get('polling_title');
            $polling->polling_details         = $request->get('polling_details');
            $polling->meta_title           = $request->get('meta_title');
            $polling->meta_description     = $request->get('meta_description');
            
            $affected_row = $polling->save();

            if (!empty($affected_row)) {
                return Response::json(['status' => 'success', 'message' => 'Data polling berhasil diupdate.', 'polling_id' => $polling->id]);
            } else {
                return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
            }
        }
        return Response::json(['status' => 'errors', 'message' => $validator->errors()]);
    }


    public function upload_image_polling(Request $request)
    {
        // dd($request->get('polling_id'));
        // dd($request->file('polling_image'));
        if ($request->hasFile('polling_image')) {
            $polling_id = $request->get('polling_id');
            // dd($request->file('polling_image'));
            $image = $request->file('polling_image');
            
            $filename = $polling_id . '-' . $image->getClientOriginalName();
            // $filename = $id . '.' . $image->getClientOriginalExtension();
            // dd($filename);
            $location = get_banner_image_path($filename);
            // create new image with transparent background color
            $background = Image::canvas(1000, 382);
            // read image file and resize it to 200x200
            $img = Image::make($image);
            // Image Height
            $height = $img->height();
            // Image Width
            $width = $img->width();
            $x = NULL;
            $y = NULL;
            if ($width > $height) {
                $y = 1000;
            } else {
                $x = 382;
            }
            //Resize Image
            $img->resize($x, $y, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            // insert resized image centered into background
            $background->insert($img, 'center');
            // save
            $background->save($location);

            if($background){
                $location_thumbnail = get_banner_thumbnail_path($filename);
                // create new image with transparent background color
                $background_thumbnail = Image::canvas(600, 300);
                // read image file and resize it to 200x200
                $img_thumbnail = Image::make($image);
                // Image Height
                $height_thumbnail = $img_thumbnail->height();
                // Image Width
                $width_thumbnail = $img_thumbnail->width();
                $x_thumbnail = NULL;
                $y_thumbnail = NULL;
                if ($width_thumbnail > $height_thumbnail) {
                    $y_thumbnail = 600;
                } else {
                    $x_thumbnail = 300;
                }
                //Resize Image
                $img_thumbnail->resize($x_thumbnail, $y_thumbnail, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
                // insert resized image centered into background_thumbnail
                $background_thumbnail->insert($img_thumbnail, 'center');
                // save
                $background_thumbnail->save($location_thumbnail);
            }


            return Response::json(['success' => true, 'error' => null, 'file' => array('name'=>$filename), 'url' => get_banner_image_url($filename)]);

        }

        return Response::json(['success' => false, 'error' => null, 'url' => '']);
    }

    public function delete_image_donasi(Request $request, $id){
        // dd($request->get('filename'));

        $filename = $request->get('filename');
        if($filename){

            @unlink(get_featured_image_path($filename));


            $affected_row = DonasiGambar::where('donasi_image',$filename)
                            ->where('user_id',$id)
                            ->delete();

            if (!empty($affected_row)) {
                return Response::json(['success' => true, 'error' => null, 'url' => '']);
            } else {
                return Response::json(['success' => false, 'error' => null, 'url' => '']);
            }
        }
        
        return Response::json(['success' => false, 'error' => null, 'url' => '']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function remove($id,$status){
        $polling = Polling::find($id);

        $polling->publication_status        = $status;
        $affected_row = $polling->save();

        if (!empty($affected_row)) {
            return Response::json(['status' => 'success', 'message' => 'Data polling berhasil diupdate.']);
        } else {
            return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
        }
    }
    public function destroy($id)
    {
        $polling = Polling::find($id);
        if ($polling != null) {
            $polling->delete();
            return Response::json(['status' => 'success', 'message' => 'Data polling berhasil didelete.']);
        } else {
            return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
        }
    }
}
