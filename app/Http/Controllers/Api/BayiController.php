<?php

namespace App\Http\Controllers\api;

use App\Bayi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class BayiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['anak'] = Bayi::where('user_id', Auth::user()->id)->paginate(15);
        
        return $data;
    }

    public function bayi_list(){
        $data['anak'] = Bayi::where('user_id', Auth::user()->id)->get();
        return $data;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'bayi_avatar'       => 'nullable',
            'bayi_name'         => 'required|min:3',
            'bayi_gender'       => 'max:250',
            'bayi_tgl_lahir'    => 'date|max:250',
            'bayi_tpt_lahir'    => 'max:250',
        ], [
            'bayi_name.required'  => 'Nama Bayi harus diisi.',
        ]);

        if ($validator->passes()) {
            $bayi = new Bayi();
            $bayi->user_id          = Auth::user()->id;
            $bayi->bayi_name        = $request->get('bayi_name');
            $bayi->bayi_nik         = $request->get('bayi_nik');
            $bayi->bayi_gender      = $request->get('bayi_gender');
            $bayi->bayi_tgl_lahir   = $request->get('bayi_tgl_lahir');
            $bayi->bayi_tpt_lahir   = $request->get('bayi_tpt_lahir');
            
            $affected_row = $bayi->save();

            if (!empty($affected_row)) {
                return Response::json(['status' => 'success', 'message' => 'Data bayi berhasil disimpan.', 'bayi_id' => $bayi->id]);
            } else {
                return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
            }
        }
        return Response::json(['status' => 'errors', 'message' => $validator->errors()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $bayi = Bayi::find($id);
        return json_encode($bayi);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $bayi = Bayi::find($id);

        $validator = Validator::make($request->all(), [
            'bayi_avatar'       => 'nullable',
            'bayi_name'         => 'required|min:3',
            'bayi_gender'       => 'max:250',
            'bayi_tgl_lahir'    => 'date|max:250',
            'bayi_tpt_lahir'    => 'max:250',
        ], [
            'bayi_name.required'  => 'Nama Bayi harus diisi.',
        ]);

        if ($validator->passes()) {
            $bayi->bayi_name        = $request->get('bayi_name');
            $bayi->bayi_nik         = $request->get('bayi_nik');
            $bayi->bayi_gender      = $request->get('bayi_gender');
            $bayi->bayi_tgl_lahir   = $request->get('bayi_tgl_lahir');
            $bayi->bayi_tpt_lahir   = $request->get('bayi_tpt_lahir');
            
            $affected_row = $bayi->save();

            if (!empty($affected_row)) {
                return Response::json(['status' => 'success', 'message' => 'Data bayi berhasil diupdate.', 'bayi_id' => $bayi->id]);
            } else {
                return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
            }
        }
        return Response::json(['status' => 'errors', 'message' => $validator->errors()]);
    }

    public function remove($id,$status){
        $bayi = Bayi::find($id);

        $bayi->activation_status        = $status;
        $affected_row = $bayi->save();

        if (!empty($affected_row)) {
            return Response::json(['status' => 'success', 'message' => 'Data bayi berhasil diupdate.']);
        } else {
            return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bayi = Bayi::find($id);
        if ($bayi != null) {
            $bayi->delete();
            return Response::json(['status' => 'success', 'message' => 'Data anak berhasil didelete.']);
        } else {
            return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
        }
    }
}
