<?php

namespace App\Http\Controllers\Api;

use App\Donasi;
use App\User;
use App\UserAlamat;
use App\DonasiBayi;
use App\DonasiOrder;
use App\DonasiKategori;
use App\DonasiGambar;
use App\Village;
use App\District;
use App\Regency;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Image;
use Purifier;

class DonasiController extends Controller
{

    public function donasi_order(Request $request){

        $data['orders'] = DonasiOrder::with(['donasi_bayi' => function($query) use($request) {
                                        $query->where('req_status', $request->get('req_status'));
                                    }, 'donasi_bayi.donasi', 'donasi_bayi.donasi.donasi_kategori', 'donasi_bayi.bayi','donasi_bayi.donasi.donasi_gambar'])
                                    ->whereHas('donasi_bayi', function($query) use($request) {
                                        $query->where('req_status', $request->get('req_status'));
                                    })
                                    ->where('user_id', Auth::user()->id)
                                    ->orderBy('created_at', 'desc')
                                    ->paginate(10);

        return $data;
    }


    public function donasi_request(Request $request){

        $data['orders'] = DonasiBayi::with(['donasi_order','donasi', 'donasi.donasi_kategori', 'bayi'])
                                    ->whereHas('donasi_order', function($query) use($request) {
                                        $query->where('pendonor_id', Auth::user()->id);
                                    })
                                    ->where('req_status', $request->get('req_status'))
                                    ->orderBy('created_at', 'desc')
                                    ->paginate(10);

        return $data;
    }

    public function donasi_request_approve(Request $request, $id){
        $donasi_bayi    = DonasiBayi::find($id);
        $donasi_bayi->req_status    = 'diterima';
        
        $affected_row   = $donasi_bayi->save();

        if (!empty($affected_row)) {
            $donasi_id              = $donasi_bayi->donasi_id;
            $req_stock              = $donasi_bayi->req_stock;

            $donasi                 = Donasi::find($donasi_id);

            $donasi_stock           = $donasi->donasi_stock;
            $sisah_stock            = $donasi_stock - $req_stock;

            $donasi->donasi_stock   = $sisah_stock;
            
            $affected_donasi        = $donasi->save();
            
            if (!empty($affected_donasi)) {

                DonasiBayi::where('req_stock','>',$sisah_stock)
                            ->where('donasi_id',$donasi_id)
                            ->where('id','!=',$id)
                            ->update(['req_status' => 'ditolak']);

            }

            return Response::json(['status' => 'success', 'message' => 'Data donasi berhasil diterima.']);
        } else {
            return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
        }


    }

    public function donasi_request_send(Request $request, $id){
        $donasi_bayi    = DonasiBayi::find($id);
        $donasi_bayi->req_status    = 'dikirim';
        
        $affected_row   = $donasi_bayi->save();

        if (!empty($affected_row)) {
            return Response::json(['status' => 'success', 'message' => 'Data donasi berhasil dikirim.']);
        } else {
            return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
        }
    }

    public function donasi_request_reject(Request $request, $id){
        $donasi_bayi    = DonasiBayi::find($id);
        $donasi_bayi->req_status    = 'ditolak';
        
        $affected_row   = $donasi_bayi->save();

        if (!empty($affected_row)) {
            return Response::json(['status' => 'success', 'message' => 'Data donasi berhasil ditolak.']);
        } else {
            return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
        }
    }

    public function donasi_order_detail($id){
        $data = DonasiBayi::where('donasi_bayi.id',$id)
                                ->leftJoin('bayi', 'bayi.id', '=', 'donasi_bayi.bayi_id')
                                ->leftJoin('users', 'users.id', '=', 'bayi.user_id')
                                ->leftJoin('indoregion_provinces', 'indoregion_provinces.id', '=', 'users.tpt_lahir')
                                ->select(array('donasi_bayi.*', 'bayi.bayi_name','bayi.bayi_gender','users.name as user_name', 'users.avatar', 'users.phone', 'users.address','indoregion_provinces.name as province_name'))
                                ->first();

        return $data;
    }

    public function donasi_order_store(Request $request){
        $validator = Validator::make($request->all(), [
            'pendonor_id'   => 'required',
            'bayi_id'       => 'required',
            'donasi_id'     => 'required',
            'req_details'   => 'required|min:3',
            'req_stock'     => 'integer|required',
        ],[
            'bayi_id.required'       => 'Nama anak harus dipilih !',
            'req_details.required'   => 'Pesan ini harus diisi !',

        ]);

        if ($validator->passes()) {
            $donasi_order = DonasiOrder::where('user_id',Auth::user()->id)
                                        ->where('pendonor_id', $request->get('pendonor_id'))
                                        ->where('order_number', null)
                                        ->first();
            if($donasi_order){
                $donasi_order_id = $donasi_order->id;
            }
            else{
                $donasi_order = new DonasiOrder();
                $donasi_order->user_id      = Auth::user()->id;
                $donasi_order->pendonor_id  = $request->get('pendonor_id');
                $affected_row = $donasi_order->save();
                
                $donasi_order_id = $donasi_order->id;
            }

            $donasi_bayi = new DonasiBayi();

            $donasi_bayi->donasi_order_id   = $donasi_order_id;
            $donasi_bayi->bayi_id           = $request->get('bayi_id');
            $donasi_bayi->donasi_id         = $request->get('donasi_id');
            $donasi_bayi->req_details       = $request->get('req_details');
            $donasi_bayi->req_stock         = $request->get('req_stock');
            $donasi_bayi->req_status        = 'order';

            $affected_row = $donasi_bayi->save();

            // dd($donasi_order_id);
            if (!empty($affected_row)) {
                return Response::json(['status' => 'success', 'message' => 'Barang berhasil ditambah di cart.']);
            } else {
                return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
            }
        }
        return Response::json(['status' => 'errors', 'message' => $validator->errors()]);
    }

    public function status_order(Request $request, $id)
    {
        $donasi_bayi = DonasiBayi::find($id);

        $donasi_bayi->req_status  = $request->get('req_status');
        
        $affected_row = $donasi_bayi->save();

        if (!empty($affected_row)) {
            return Response::json(['status' => 'success', 'message' => 'Data status order berhasil diupdate.']);
        } else {
            return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
        }

    }

    public function donasi_cart()
    {
        $data['count'] = DonasiBayi::whereHas('donasi_order', function($query){
                                        $query->where('user_id', Auth::user()->id);
                                    })
                                    ->where('req_status', 'order')
                                    ->count();


        $data['orders'] = DonasiBayi::with(['donasi', 'donasi.donasi_kategori', 'donasi.donasi_gambar'])
                                    ->whereHas('donasi_order', function($query){
                                        $query->where('user_id', Auth::user()->id);
                                    })
                                    ->where('req_status', 'order')
                                    ->limit(3)
                                    ->orderBy('created_at', 'desc')
                                    ->get();
        return $data;
    }

    public function donasi_checkout()
    {
        $data['orders'] = DonasiOrder::with(['pendonor', 'pendonor.logistics','donasi_bayi'  => function($query) {
                                        $query->where('req_status', 'order');
                                    },'donasi_bayi.donasi', 'donasi_bayi.donasi.donasi_kategori', 'donasi_bayi.donasi.donasi_gambar'])
                                    ->whereHas('donasi_bayi.donasi_order', function($query){
                                        $query->where('req_status', 'order');
                                    })
                                    ->leftJoin('users', 'users.id', '=', 'donasi_order.pendonor_id')
                                    ->leftJoin('user_logistic' , function ($query) {
                                        $query->on('user_logistic.id', '=', DB::raw('(SELECT id FROM user_logistic WHERE user_logistic.user_id = users.id ORDER BY user_logistic.logistic_id ASC LIMIT 1)'));
                                    })
                                    ->leftJoin('logistics', 'logistics.id', '=', 'user_logistic.logistic_id')
                                    ->where('donasi_order.user_id', Auth::user()->id)
                                    ->select(array('donasi_order.*', 'users.name', 'user_logistic.logistic_id', 'logistics.display_name', 'logistics.default_price'))
                                    ->orderBy('donasi_order.created_at', 'desc')
                                    ->get();


        $data['alamat'] = UserAlamat::where('users_alamat.user_id', Auth::user()->id)
                            ->where('users_alamat.activation_status', 1)
                            ->leftJoin('indoregion_villages', 'indoregion_villages.id', '=', 'users_alamat.village_id')
                            ->leftJoin('indoregion_districts', 'indoregion_districts.id', '=', 'indoregion_villages.district_id')
                            ->leftJoin('indoregion_regencies', 'indoregion_regencies.id', '=', 'indoregion_districts.regency_id')
                            ->leftJoin('indoregion_provinces', 'indoregion_provinces.id', '=', 'indoregion_regencies.province_id')
                            ->select(array('users_alamat.*','indoregion_villages.name as vilage_name','indoregion_districts.name as district_name', 'indoregion_regencies.name as regency_name', 'indoregion_provinces.name as province_name'))
                            ->first();

/*
        $data['donasi'] = Donasi::with(['donasi_kategori'])
                                    ->leftJoin('users', 'users.id', '=', 'donasi.user_id')
                                    ->where('donasi.user_id', Auth::user()->id)
                                    ->orderBy('donasi.created_at', 'desc')
                                    ->select(array('donasi.donasi_name', 'users.name','donasi.donasi_kategori_id'))
                                    ->get();*/
        return $data;
    }

    public function donasi_checkout_update(Request $request)
    {

        $newValues = $request->get('donasi_order');

        if($newValues){
            foreach ($newValues as $key => $value) {
                // dd($value['id']);

                $donasi_order = DonasiOrder::find($value['id']);

                $donasi_order->logistic_id      = $value['logistic_id'];
                $donasi_order->user_alamat_id   = $value['user_alamat_id'];
                $donasi_order->total_price      = $value['total_price'];
                $donasi_order->order_number     = $this->get_ordernumber();
                
                $affected_row = $donasi_order->save();

                if (!empty($affected_row)) {

                    DonasiBayi::where('donasi_order_id',$donasi_order->id)
                                ->where('req_status','order')
                                ->update(['req_status' => 'menunggu']);

                    return Response::json(['status' => 'success', 'message' => 'Data stock berhasil diupdate.']);
                } else {
                    return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
                }

            }
        }
        else {
            return Response::json(['status' => 'errors', 'message' => 'Tidak ada data pesanan']);
        }
    }

    public function stock_update(Request $request, $id){


        $donasi_bayi = DonasiBayi::find($id);

        $donasi_bayi->req_stock   = $request->get('req_stock');
        
        $affected_row = $donasi_bayi->save();

        if (!empty($affected_row)) {
            return Response::json(['status' => 'success', 'message' => 'Data stock berhasil diupdate.']);
        } else {
            return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
        }

    }

    public function stock_delete($id)
    {
        $donasi_bayi = DonasiBayi::find($id);
        if ($donasi_bayi != null) {
            $donasi_bayi->delete();
            return Response::json(['status' => 'success', 'message' => 'Data stock berhasil didelete.']);
        } else {
            return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
        }
    }
    public function get_ordernumber()
    {
        // Get the last created order
        $lastOrder = DonasiOrder::where('order_number','!=',null)->orderBy('created_at', 'desc')->first();


        if ( ! $lastOrder ){
            $number = 0;
        }
        else {
            $number = substr($lastOrder->order_number, 3);
        }

        $order_number = 'ORD' . sprintf('%06d', intval($number) + 1);

        $validator = Validator::make(
            [
                'order_number'          => $order_number,
            ], [
                'order_number'          => 'unique:donasi_order',
            ], [
                'order_number.unique'   => 'Order Number Sudah Ada.',
            ]);


         if($validator->fails()){
              return $this->get_ordernumber();
         }


        // If we have ORD000001 in the database then we only want the number
        // So the substr returns this 000001

        // Add the string in front and higher up the number.
        // the %05d part makes sure that there are always 6 numbers in the string.
        // so it adds the missing zero's when needed.
        return $order_number;
    }


    public function donasi_kategori()
    {
        $data['donasi_kategori'] = DonasiKategori::where('parent_kategori_id', '!=', NULL)->get();

        return json_encode($data);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['donasi'] = Donasi::where('user_id', Auth::user()->id)->orderBy('donasi_date', 'desc')->paginate(15);
        
        return $data;
    }

    public function group(Request $request, $name)
    {
        // dd($request->get('donasi_kategori_id'));
        if($name == 'donasi'){
            $data['donasi'] = Donasi::with('donasi_gambar', 'donasi_kategori')
                                    ->where('publication_status', 1)
                                    ->where('donasi_stock','>', 0)
                                    ->when(!empty($request->get('donasi_kategori_id') && $request->get('donasi_kategori_id') != 'null') , function ($query) use($request){
                                        $query->whereHas('donasi_kategori', function($query) use($request) {
                                            $query->where('id', $request->get('donasi_kategori_id'));
                                        });
                                    })
                                    ->when(!empty($request->get('propinsi_id') && $request->get('propinsi_id') != 'null') , function ($query) use($request){
                                        $query->whereHas('user.user_alamat', function($query) use($request) {
                                            $query->where('activation_status', 1);
                                            $query->whereHas('village.district.regency', function($query) use($request) {
                                                $query->where('province_id', $request->get('propinsi_id'));
                                            });
                                        });
                                    })
                                    ->whereHas('user.user_alamat', function($query) use($request) {
                                        $query->where('activation_status', 1);
                                    })
                                    ->orderBy('donasi_date', 'desc')
                                    ->paginate(16);
        }
        else if($name == 'home'){
            $data['donasi']['data'] = Donasi::with('donasi_gambar', 'donasi_kategori')
                                    ->where('publication_status', 1)
                                    ->where('donasi_stock','>', 0)
                                    ->whereHas('user.user_alamat', function($query) use($request) {
                                        $query->where('activation_status', 1);
                                    })
                                    ->orderBy('donasi_date', 'desc')
                                    ->limit(8)
                                    ->get();
        }
        else{
            $data['donasi']['data'] = Donasi::with('donasi_gambar', 'donasi_kategori')
                                    ->when(!empty($request->get('donasi_id')) , function ($query) use($request) {
                                        $query->where('id','!=', $request->get('donasi_id'));
                                    })
                                    ->where('publication_status', 1)
                                    ->where('donasi_stock','>', 0)
                                    ->whereHas('user.user_alamat', function($query) use($request) {
                                        $query->where('activation_status', 1);
                                    })
                                    // ->orderBy('donasi_date', 'desc')
                                    ->inRandomOrder()
                                    ->limit(4)
                                    ->get();
        }
        // dd($data);
        
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'donasi_kategori_id'    => 'required',
            'donasi_name'           => 'required|min:3',
            'donasi_details'        => 'required|min:3',
            'donasi_stock'          => 'integer|required',
        ]);

        if ($validator->passes()) {
            $donasi = new Donasi();
            $donasi->user_id              = Auth::user()->id;
            $donasi->donasi_kategori_id   = $request->get('donasi_kategori_id');
            $donasi->donasi_name          = $request->get('donasi_name');
            $donasi->donasi_slug          = str_replace(' ', '-', $request->get('donasi_name'));
            $donasi->donasi_details       = $request->get('donasi_details');
            $donasi->donasi_stock         = $request->get('donasi_stock');
            $donasi->meta_title           = $request->get('donasi_name');
            $donasi->meta_keywords        = str_replace(' ', '-', $request->get('donasi_name'));
            $donasi->meta_description     = $request->get('donasi_details');
            
            $affected_row = $donasi->save();

            if (!empty($affected_row)) {
                return Response::json(['status' => 'success', 'message' => 'Data donasi berhasil disimpan.', 'donasi_id' => $donasi->id]);
            } else {
                return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
            }
        }
        return Response::json(['status' => 'errors', 'message' => $validator->errors()]);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['item'] = Donasi::with(['donasi_gambar','user',
                                    'user.user_alamat'  => function($query) {
                                        $query->where('activation_status', 1);
                                    },'user.user_alamat.village.district.regency.province'])
                                    ->find($id);
        return $data;
    }

    public function show_edit_donasi($id)
    {
        $data['donasi'] = Donasi::where('user_id', Auth::user()->id)->find($id);
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $donasi = Donasi::find($id);
        $validator = Validator::make($request->all(), [
            'donasi_kategori_id'    => 'required',
            'donasi_name'           => 'required|min:3',
            'donasi_details'        => 'required|min:3',
            'donasi_stock'          => 'integer|required',
        ]);

        if ($validator->passes() && $donasi->user_id == Auth::user()->id) {
            $donasi->donasi_kategori_id   = $request->get('donasi_kategori_id');
            $donasi->donasi_name          = $request->get('donasi_name');
            $donasi->donasi_slug          = str_replace(' ', '-', $request->get('donasi_name'));
            $donasi->donasi_details       = $request->get('donasi_details');
            $donasi->donasi_stock         = $request->get('donasi_stock');
            $donasi->meta_title           = $request->get('donasi_name');
            $donasi->meta_keywords        = str_replace(' ', '-', $request->get('donasi_name'));
            $donasi->meta_description     = $request->get('donasi_details');
            
            $affected_row = $donasi->save();

            if (!empty($affected_row)) {
                return Response::json(['status' => 'success', 'message' => 'Data donasi berhasil diupdate.', 'donasi_id' => $donasi->id]);
            } else {
                return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
            }
        }
        return Response::json(['status' => 'errors', 'message' => $validator->errors()]);
    }

    public function get_image_donasi($id){

        $data['donasi'] = DonasiGambar::where('donasi_id', $id)->get();
        
        return $data;
    }

    public function upload_image_donasi(Request $request)
    {
        // dd($request->get('donasi_id'));
        if ($request->hasFile('donasi_image')) {
            $donasi_id = $request->get('donasi_id');
            // dd($request->file('donasi_image'));
            $image = $request->file('donasi_image');
            
            $filename = $donasi_id . '-' . $image->getClientOriginalName();
            // $filename = $id . '.' . $image->getClientOriginalExtension();
            // dd($filename);
            $location = get_featured_image_path($filename);
            // create new image with transparent background color
            $background = Image::canvas(600, 600);
            // read image file and resize it to 200x200
            $img = Image::make($image);
            // Image Height
            $height = $img->height();
            // Image Width
            $width = $img->width();
            $x = NULL;
            $y = NULL;
            if ($width > $height) {
                $y = 600;
            } else {
                $x = 600;
            }
            //Resize Image
            $img->resize($x, $y, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            // insert resized image centered into background
            $background->insert($img, 'center');
            // save
            $background->save($location);


            $donasigambar = new DonasiGambar();
            $donasigambar->donasi_id    = $donasi_id;
            $donasigambar->donasi_image = $filename.'?t='.rand();
            
            $affected_row = $donasigambar->save();

            if (!empty($affected_row)) {
                return Response::json(['success' => true, 'error' => null, 'file' => array('name'=>$filename), 'url' => get_featured_image_url($filename)]);
            } else {
                return Response::json(['success' => false, 'error' => null, 'url' => '']);
            }

        }

        return Response::json(['success' => false, 'error' => null, 'url' => '']);
    }

    public function delete_image_donasi(Request $request, $id){
        // dd($request->get('filename'));

        $filename = $request->get('filename');
        if($filename){

            @unlink(get_featured_image_path($filename));


            $affected_row = DonasiGambar::where('donasi_image',$filename)
                            ->where('donasi_id',$id)
                            ->delete();

            if (!empty($affected_row)) {
                return Response::json(['success' => true, 'error' => null, 'url' => '']);
            } else {
                return Response::json(['success' => false, 'error' => null, 'url' => '']);
            }
        }
        
        return Response::json(['success' => false, 'error' => null, 'url' => '']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function remove($id,$status){
        $donasi = Donasi::find($id);

        $donasi->publication_status        = $status;
        $affected_row = $donasi->save();

        if (!empty($affected_row)) {
            return Response::json(['status' => 'success', 'message' => 'Data donasi berhasil diupdate.']);
        } else {
            return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
        }
    }
    
    public function destroy($id)
    {
        $donasi = Donasi::find($id);
        if ($donasi != null) {
            $donasi->delete();
            return Response::json(['status' => 'success', 'message' => 'Data donasi berhasil didelete.']);
        } else {
            return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
        }
    }
}
