<?php

namespace App\Http\Controllers\api;

use App\Bayi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class SilsilahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function get_silsilah($jenis){
        if($jenis == 'anak'){
            $data = Bayi::where('bayi.user_id', Auth::user()->id)
                        ->leftJoin('indoregion_provinces', 'indoregion_provinces.id', '=', 'bayi.bayi_tpt_lahir')
                        ->leftJoin('users', 'users.id', '=', 'bayi.user_id')
                        ->leftJoin('indoregion_provinces as user_prov', 'user_prov.id', '=', 'users.tpt_lahir')
                        ->select(array('bayi.bayi_avatar','bayi.bayi_gender','bayi.bayi_name','bayi.bayi_tgl_lahir','indoregion_provinces.name as bayi_tempat_lahir', 'users.agama', 'users.avatar', 'users.email', 'users.facebook', 'users.twitter', 'users.gender', 'users.name', 'users.phone', 'user_prov.name as user_tempat_lahir'))
                        ->paginate(6);

        }
        elseif($jenis == 'sesusuan'){
            $data = Bayi::leftJoin('indoregion_provinces', 'indoregion_provinces.id', '=', 'bayi.bayi_tpt_lahir')
                        ->leftJoin('users', 'users.id', '=', 'bayi.user_id')
                        ->leftJoin('indoregion_provinces as user_prov', 'user_prov.id', '=', 'users.tpt_lahir')
                        ->select(array('bayi.bayi_avatar','bayi.bayi_gender','bayi.bayi_name','bayi.bayi_tgl_lahir','indoregion_provinces.name as bayi_tempat_lahir', 'users.agama', 'users.avatar', 'users.email', 'users.facebook', 'users.twitter', 'users.gender', 'users.name', 'users.phone', 'user_prov.name as user_tempat_lahir'))
                        ->where(function($query){
                            $query->where(function($query){
                                $query->whereIn('bayi.id', 
                                    Bayi::select('bayi.id')
                                        ->leftJoin('donasi_bayi', 'donasi_bayi.bayi_id', '=', 'bayi.id')
                                        ->leftJoin('donasi', 'donasi.id', '=', 'donasi_bayi.donasi_id')
                                        ->where('donasi.user_id', Auth::user()->id)
                                        ->where('donasi.donasi_kategori_id', 2)
                                        ->where('donasi_bayi.req_status', 'selesai')
                                        ->where('bayi.user_id','!=', Auth::user()->id)
                                        ->get()
                                );
                            });
                            $query->orWhere(function($query){
                                $query->whereIn('bayi.id', 
                                    Bayi::select('bayi.id')
                                        ->whereIn('bayi.user_id', 
                                            Bayi::select('donasi.user_id')
                                                ->leftJoin('donasi_bayi', 'donasi_bayi.bayi_id', '=', 'bayi.id')
                                                ->leftJoin('donasi', 'donasi.id', '=', 'donasi_bayi.donasi_id')
                                                ->where('donasi.donasi_kategori_id', 2)
                                                ->where('donasi_bayi.req_status', 'selesai')
                                                ->where('bayi.user_id', Auth::user()->id)
                                                ->get()
                                        )
                                        ->where('bayi.user_id','!=', Auth::user()->id)
                                        ->get()
                                );
                            });
                        })
                        ->paginate(6);
        }

        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
