<?php

namespace App\Http\Controllers\api;

use App\Province;
use App\Regency;
use App\District;
use App\Village;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class WilayahController extends Controller
{
    public function index_propinsi(){
        $data['propinsi'] = Province::get();
        
        return $data;
    }
    
    public function index_kabupaten($province_id){
        $data['kabupaten'] = Regency::where('province_id', $province_id)
        							->get();
        return $data;
    }
    
    public function index_kecamatan($regency_id){
        $data['kecamatan'] = District::where('regency_id', $regency_id)
        							->get();
        
        return $data;
    }
    
    public function index_keluarahan($district_id){
        $data['kelurahan'] = Village::where('district_id', $district_id)
        							->get();
        
        return $data;
    }
}
