<?php

namespace App\Http\Controllers\api;

use App\User;
use App\Bayi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Image;
use Purifier;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function upload_image(){

    }

    public function get_avatar($name, $id){
        if($name == 'user'){
            $data  = User::select('avatar')->find($id);

        }else{
            $data  = Bayi::find($id);
        }

        return $data;
    }

    public function upload_avatar(Request $request){
        // dd($request->get('name'));
        // dd($request->file('avatar'));
        if ($request->hasFile('avatar')) {
            $name = $request->get('name');
            $user_id = $request->get('user_id');
            $img_name = $name.'-'.$user_id;
            // dd($user_id);
            $image = $request->file('avatar');
            
            $filename = $img_name . '-' . $image->getClientOriginalName();
            // $filename = $id . '.' . $image->getClientOriginalExtension();
            // dd($filename);
            $location = get_avatar_image_path($filename);
            // create new image with transparent background color
            $background = Image::canvas(400, 400);
            // read image file and resize it to 200x200
            $img = Image::make($image);
            // Image Height
            $height = $img->height();
            // Image Width
            $width = $img->width();
            $x = NULL;
            $y = NULL;
            if ($width > $height) {
                $y = 400;
            } else {
                $x = 400;
            }
            //Resize Image
            $img->resize($x, $y, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            // insert resized image centered into background
            $background->insert($img, 'center');
            // save
            $background->save($location);

            if($name == 'user'){
                $user_avatar                = User::find($user_id);
                $user_avatar->avatar        = $filename.'?t='.rand();
                $affected_row               = $user_avatar->save();

            }else{
                $bayi_avatar                = Bayi::find($user_id);
                $bayi_avatar->bayi_avatar   = $filename.'?t='.rand();
                $affected_row               = $bayi_avatar->save();
            }


            if (!empty($affected_row)) {
                return Response::json(['success' => true, 'error' => null, 'file' => array('name'=>$filename), 'url' => get_banner_image_url($filename)]);
            } else {
                return Response::json(['success' => false, 'error' => null, 'url' => '']);
            }

        }

        return Response::json(['success' => false, 'error' => null, 'url' => '']);

    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $user = User::where('id', $id)->first();
        return json_encode($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        if ($user->nik == $request->nik) {
            $nik = "required|min:15";
        } else {
            $nik = "required|min:15|unique:users";
        }
        if ($user->username == $request->username) {
            $username = "required|min:3";
        } else {
            $username = "required|min:3|unique:users";
        }
        if ($user->email == $request->email) {
            $email = "required|email";
        } else {
            $email = "required|email|unique:users";
        }

        $validator = Validator::make($request->all(), [
            'name'          => 'required|min:3',
            'nik'           => $nik,
            'username'      => $username,
            'email'         => $email,
            'gender'        => 'max:250',
            'phone'         => 'max:250',
            'address'       => 'max:250',
            'tpt_lahir'     => 'max:250',
            'tgl_lahir'     => 'date|max:250',
            'agama'         => 'max:250',
        ], [
            'nik.required'  => 'NIK harus diisi.',
        ]);

        if ($validator->passes()) {
            $user->name         = $request->get('name');
            $user->nik          = $request->get('nik');
            $user->username     = $request->get('username');
            $user->email        = $request->get('email');
            $user->gender       = $request->get('gender');
            $user->phone        = $request->get('phone');
            $user->address      = $request->get('address');
            $user->tpt_lahir    = $request->get('tpt_lahir');
            $user->tgl_lahir    = $request->get('tgl_lahir');
            $user->agama        = $request->get('agama');
            $user->kode_pos     = $request->get('kode_pos');

            $affected_row = $user->save();

            if (!empty($affected_row)) {
                return Response::json(['status' => 'success', 'message' => 'User update successfully.']);
            } else {
                return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
            }
        }
        return Response::json(['status' => 'errors', 'message' => $validator->errors()]);
    }

    public function update_usertour(Request $request, $id){

        $tour = User::find($id);

        $tour->web_tour  = $request->get('web_tour');
        
        $affected_row = $tour->save();

        if (!empty($affected_row)) {
            return Response::json(['status' => 'success', 'message' => 'Data user web tour terupdate.']);
        } else {
            return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
        }

    }

    public function ubah_password(Request $request){

        $validator = Validator::make($request->all(), [
            'password_old'  => 'required|min:3',
            'password'      => 'required|min:3|confirmed',
        ], [
            'password.confirmed'  => 'Password konfirmasi anda tidak sesuai.',
        ]);

        if ($validator->passes()) {
            $credentials = ['id' => Auth::user()->id, 'password' => $request->password_old, 'activation_status' => 1];
            // $credentials = $request->only('email', 'password');
            if ($token = $this->guard()->attempt($credentials)) {
                // dd(Auth::user()->activation_status);
                $user               = User::find(Auth::user()->id);
                $user->password     = bcrypt($request->password);
                $affected_row       = $user->save();

                if (!empty($affected_row)) {
                    return Response::json(['status' => 'success', 'message' => 'Data password berhasil diupdate.']);
                } else {
                    return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
                }

            }

            return Response::json(['status' => 'errors', 'message' => array('password_old'=>'[ "Maaf, password anda salah !." ]')]);

        }
        return Response::json(['status' => 'errors', 'message' => $validator->errors()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * Return auth guard
     */
    private function guard()
    {
        return Auth::guard();
    }
}
