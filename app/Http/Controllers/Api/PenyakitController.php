<?php

namespace App\Http\Controllers\api;

use App\User;
use App\Penyakit;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class PenyakitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['penyakit'] = Penyakit::get();
        
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'penyakit_name'         => 'required|min:3|unique:penyakit',
        ], [
            'penyakit_name.unique'      => 'Nama penyakit sudah ada.',
            'penyakit_name.required'    => 'Nama penyakit harus diisi.',
        ]);

        if ($validator->passes()) {
            $penyakit = new Penyakit();
            $penyakit->penyakit_name        = $request->get('penyakit_name');
            
            $affected_row = $penyakit->save();

            if (!empty($affected_row)) {
                return Response::json(['status' => 'success', 'message' => 'Data penyakit berhasil disimpan.']);
            } else {
                return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
            }
        }
        return Response::json(['status' => 'errors', 'message' => $validator->errors()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $penyakit = Penyakit::find($id);
        return json_encode($penyakit);
    }


    public function penyakit_user($id)
    {
        $data['user'] = User::where('id', $id)->first();
        $data['penyakits'] = $data['user']->penyakits()->pluck('penyakit_id');

        return json_encode($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $penyakit = Penyakit::find($id);

        $validator = Validator::make($request->all(), [
            'penyakit_name'         => 'required|min:3',
        ], [
            'penyakit_name.required'  => 'Nama penyakit harus diisi.',
        ]);

        if ($validator->passes()) {
            $penyakit->penyakit_name        = $request->get('penyakit_name');
            
            $affected_row = $penyakit->save();

            if (!empty($affected_row)) {
                return Response::json(['status' => 'success', 'message' => 'Data penyakit berhasil diupdate.']);
            } else {
                return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
            }
        }
        return Response::json(['status' => 'errors', 'message' => $validator->errors()]);
    }

    public function update_penyakit(Request $request, $id)
    {
        $user = User::find($id);

        if (isset($request->penyakit_user)) {
            $user->penyakits()->sync($request->penyakit_user);
            return Response::json(['status' => 'success', 'message' => 'Data penyakit berhasil disimpan.']);
        } else {
            $user->penyakits()->sync(array());
            return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
