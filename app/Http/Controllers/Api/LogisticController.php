<?php

namespace App\Http\Controllers\api;

use App\User;
use App\Logistic;
use App\DonasiOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class LogisticController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function user_logistic(){
        $data['user_logistic'] = Logistic::whereHas('users', function($query){
                                $query->where('users.id', Auth::user()->id);
                            })
                            ->pluck('id');

        $data['logistic'] = Logistic::get();

        return $data;
    }

    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function user_logistic_update(Request $request, $id)
    {
        $user = User::find($id);

        if (isset($request->logisitic_id)) {
            $user->logistics()->sync($request->logisitic_id);
            return Response::json(['status' => 'success', 'message' => 'Data jasa kirim berhasil disimpan.']);
        } else {
            $user->logistics()->sync(array());
            return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
