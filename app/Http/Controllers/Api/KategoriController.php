<?php

namespace App\Http\Controllers\api;

use App\Kategori;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['kategori'] = Kategori::get();

        return json_encode($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'kategori_name'       => 'required|min:3|unique:kategori',
            'kategori_desc'       => 'nullable',
        ],[

            'kategori_name.unique'  => 'Nama kategori sudah tersedia.',
        ]);

        if ($validator->passes()) {
            $kategori = new Kategori();
            $kategori->kategori_name    = $request->get('kategori_name');
            $kategori->kategori_desc    = $request->get('kategori_desc');
            
            $affected_row = $kategori->save();

            if (!empty($affected_row)) {
                return Response::json(['status' => 'success', 'message' => 'Data kategori berhasil disimpan.']);
            } else {
                return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
            }
        }
        return Response::json(['status' => 'errors', 'message' => $validator->errors()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Kategori::find($id);

        return json_encode($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
