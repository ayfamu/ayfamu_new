<?php

namespace App\Http\Controllers\Api;

use App\Info;
use App\InfoComment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Image;
use Purifier;
use App\Http\Controllers\Controller;

class InfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['info'] = Info::where('user_id', Auth::user()->id)->orderBy('info_date', 'desc')->paginate(15);
        
        return $data;
    }

    public function group(Request $request, $name)
    {
        if($name == 'info'){
            $data['info'] = Info::where('publication_status', 1)
                                    ->orderBy('info_date', 'desc')
                                    ->paginate(12);
        }else if($name == 'home'){
            // $data['donasi'] = Donasi::paginate(8);
            $data['info']['data'] = Info::where('publication_status', 1)
                                    ->orderBy('info_date', 'desc')
                                    ->limit(6)
                                    ->get();
        }else{
            // $data['donasi'] = Donasi::paginate(8);
            $data['info']['data'] = Info::where('publication_status', 1)
                                    ->when(!empty($request->get('info_id')) , function ($query) use($request) {
                                        $query->where('id','!=', $request->get('info_id'));
                                    })
                                    ->inRandomOrder()
                                    ->limit(3)
                                    ->get();
        }
        
        return $data;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'kategori_id'   => 'required',
            'info_title'    => 'required|min:3',
            'info_details'  => 'required|min:3',
        ]);

        if ($validator->passes()) {
            $info = new Info();
            $info->user_id              = Auth::user()->id;
            $info->kategori_id          = $request->get('kategori_id');
            // $info->info_image           = $request->get('info_image');
            $info->info_title           = $request->get('info_title');
            $info->info_details         = $request->get('info_details');
            $info->meta_title           = $request->get('meta_title');
            $info->meta_description     = $request->get('meta_description');
            
            $affected_row = $info->save();

            if (!empty($affected_row)) {
                return Response::json(['status' => 'success', 'message' => 'Data info berhasil disimpan.', 'info_id' => $info->id]);
            } else {
                return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
            }
        }
        return Response::json(['status' => 'errors', 'message' => $validator->errors()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['info'] = Info::leftJoin('users', 'users.id', '=', 'info.user_id')
                            ->select(array('info.*','users.name as user_name','users.avatar'))
                            ->findOrFail($id);

        return $data;
    }

    public function show_info_comment($info_id){
        $data['comment'] = InfoComment::where('info_comments.info_id',$info_id)
                                        ->orderBy('info_comments.created_at','desc')
                                        ->leftJoin('users', 'users.id', '=', 'info_comments.user_id')
                                        ->select(array('info_comments.*','users.name as user_name','users.avatar'))
                                        ->paginate(5);

        return $data;
    }

    public function store_info_comment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'comment'   => 'required',
        ],[
            'comment.required' => 'Komentar tidak boleh kosong'
        ]);

        if ($validator->passes()) {
            $comment = new InfoComment();
            $comment->user_id   = Auth::user()->id;
            $comment->info_id   = $request->get('info_id');
            $comment->comment   = $request->get('comment');
            
            $affected_row = $comment->save();

            if (!empty($affected_row)) {
                return Response::json(['status' => 'success', 'message' => 'Data comment berhasil disimpan.']);
            } else {
                return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
            }
        }
        return Response::json(['status' => 'errors', 'message' => $validator->errors()]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function show_edit_info($id)
    {
        $data['info'] = Info::where('user_id', Auth::user()->id)->find($id);
        return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $info = Info::find($id);
        $validator = Validator::make($request->all(), [
            'kategori_id'   => 'required',
            'info_title'    => 'required|min:3',
            'info_details'  => 'required|min:3',
        ]);

        if ($validator->passes() && $info->user_id == Auth::user()->id) {
            $info->kategori_id          = $request->get('kategori_id');
            $info->info_image           = $request->get('info_image').'?t='.rand();
            $info->info_title           = $request->get('info_title');
            $info->info_details         = $request->get('info_details');
            $info->meta_title           = $request->get('meta_title');
            $info->meta_description     = $request->get('meta_description');
            
            $affected_row = $info->save();

            if (!empty($affected_row)) {
                return Response::json(['status' => 'success', 'message' => 'Data info berhasil diupdate.', 'info_id' => $info->id]);
            } else {
                return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
            }
        }
        return Response::json(['status' => 'errors', 'message' => $validator->errors()]);
    }


    public function upload_image_info(Request $request)
    {
        if ($request->hasFile('info_image')) {
            $info_id = $request->get('info_id');
            $image = $request->file('info_image');
            
            $filename = $info_id . '-' . $image->getClientOriginalName();
            $location = get_banner_image_path($filename);
            // create new image with transparent background color
            $background = Image::canvas(1000, 382);
            // read image file and resize it to 200x200
            $img = Image::make($image);
            // Image Height
            $height = $img->height();
            // Image Width
            $width = $img->width();
            $x = NULL;
            $y = NULL;
            if ($width > $height) {
                $y = 1000;
            } else {
                $x = 382;
            }
            //Resize Image
            $img->resize($x, $y, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            // insert resized image centered into background
            $background->insert($img, 'center');
            // save
            $background->save($location);

            if($background){
                $location_thumbnail = get_banner_thumbnail_path($filename);
                // create new image with transparent background color
                $background_thumbnail = Image::canvas(600, 300);
                // read image file and resize it to 200x200
                $img_thumbnail = Image::make($image);
                // Image Height
                $height_thumbnail = $img_thumbnail->height();
                // Image Width
                $width_thumbnail = $img_thumbnail->width();
                $x_thumbnail = NULL;
                $y_thumbnail = NULL;
                if ($width_thumbnail > $height_thumbnail) {
                    $y_thumbnail = 600;
                } else {
                    $x_thumbnail = 300;
                }
                //Resize Image
                $img_thumbnail->resize($x_thumbnail, $y_thumbnail, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
                // insert resized image centered into background_thumbnail
                $background_thumbnail->insert($img_thumbnail, 'center');
                // save
                $background_thumbnail->save($location_thumbnail);
            }

            return Response::json(['success' => true, 'error' => null, 'file' => array('name'=>$filename), 'url' => get_banner_image_url($filename)]);

        }

        return Response::json(['success' => false, 'error' => null, 'url' => '']);
    }

    public function delete_image_donasi(Request $request, $id){
        // dd($request->get('filename'));

        $filename = $request->get('filename');
        if($filename){

            @unlink(get_featured_image_path($filename));


            $affected_row = DonasiGambar::where('donasi_image',$filename)
                            ->where('user_id',$id)
                            ->delete();

            if (!empty($affected_row)) {
                return Response::json(['success' => true, 'error' => null, 'url' => '']);
            } else {
                return Response::json(['success' => false, 'error' => null, 'url' => '']);
            }
        }
        
        return Response::json(['success' => false, 'error' => null, 'url' => '']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function remove($id,$status){
        $info = Info::find($id);

        $info->publication_status        = $status;
        $affected_row = $info->save();

        if (!empty($affected_row)) {
            return Response::json(['status' => 'success', 'message' => 'Data info berhasil diupdate.']);
        } else {
            return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
        }
    }
    public function destroy($id)
    {
        $info = Info::find($id);
        if ($info != null) {
            $info->delete();
            return Response::json(['status' => 'success', 'message' => 'Data info berhasil didelete.']);
        } else {
            return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
        }
    }
}
