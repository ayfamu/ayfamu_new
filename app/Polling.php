<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Polling extends Model
{
	protected $table = 'polling';

    protected $fillable = [
        'user_id', 'kategori_id', 'polling_image', 'polling_title', 'polling_details', 'polling_date', 'publication_status', 'meta_title', 'meta_keywords', 'meta_description'
    ];

	public function user() {
		return $this->belongsTo(User::class);
	}

	public function kategori() {
		return $this->belongsTo(Kategori::class);
	}

	public function polling_user() {
		return $this->hasMany(PollingUser::class);
	}

	public function avgRating()
	{
	    return $this->polling_user()
	      ->selectRaw('avg(polling_user.polling_count) as aggregate, polling_user.polling_id, count(polling_user.polling_count) as total_user')
	      ->groupBy('polling_user.polling_id');
	}

}
