<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => '/', 'namespace' => 'Api', 'as' => 'api.'], function () {

    Route::resource('apiweb', 'WebController', ['except' => ['create', 'edit']]);

    Route::post('apisaran', 'WebController@store_saran');

    // wilayah
    Route::get('apipropinsi', 'WilayahController@index_propinsi');
    Route::get('apikabupaten/{province_id}', 'WilayahController@index_kabupaten');
    Route::get('apikecamatan/{regency_id}', 'WilayahController@index_kecamatan');
    Route::get('apikeluarahan/{district_id}', 'WilayahController@index_keluarahan');

    // donasi
    Route::resource('apidonasi', 'DonasiController', ['only' => ['show']]);
    Route::get('apidonasigroup/{name?}', 'DonasiController@group');
    
    // donasi gambar
    Route::get('apidonasifoto/{id}', 'DonasiController@get_image_donasi');
    Route::post('apidonasifoto', 'DonasiController@upload_image_donasi');
    Route::delete('apidonasifoto/{id}', 'DonasiController@delete_image_donasi');

    // donasi bayi
    Route::get('apidonasikateogori', 'DonasiController@donasi_kategori');
    Route::get('apidonasiorderdetail/{id}', 'DonasiController@donasi_order_detail');

    
    // kategori info & polling
    Route::resource('apikateogori', 'KategoriController', ['only' => ['index', 'show']]);


    // info
    Route::resource('apiinfo', 'InfoController', ['only' => ['show']]);
    Route::get('apiinfogroup/{name?}', 'InfoController@group');
    Route::post('apiinfofoto', 'InfoController@upload_image_info');

    Route::get('apiinfocomment/{id}', 'InfoController@show_info_comment');
    // polling
    Route::resource('apipolling', 'PollingController', ['only' => ['index', 'show']]);
    Route::get('apipollinggroup/{name?}', 'PollingController@group');
    Route::post('apipollingfoto', 'PollingController@upload_image_polling');
    Route::get('apipollingedit/{id}', 'PollingController@show_edit_polling');

    // penyakit
    Route::resource('apipenyakit', 'PenyakitController', ['only' => ['index', 'show']]);
    Route::get('apipenyakituser/{id}', 'PenyakitController@penyakit_user');

    // obat
    Route::resource('apiobat', 'ObatController', ['only' => ['index', 'show']]);
    Route::get('apiobatuser/{id}', 'ObatController@obat_user');

    // user
    Route::resource('apiuser', 'UserController', ['only' => ['index', 'show']]);
    Route::get('apiavatar/{name}/{id}', 'UserController@get_avatar');
    Route::post('apiavatar', 'UserController@upload_avatar');
    Route::post('apiimage', 'UserController@upload_image');

    // alamat
    Route::resource('apialamat', 'UserAlamatController', ['only' => ['index', 'show']]);
    Route::get('apialamatuser/{id}', 'UserAlamatController@alamat_user');

    // bayi
    Route::resource('apibayi', 'BayiController', ['only' => ['show']]);


    Route::prefix('auth')->group(function () {

	    Route::post('register', 'AuthController@register');
	    // Login User
	    Route::post('login', 'AuthController@login');
	    // Refresh the JWT Token
	    Route::get('refresh', 'AuthController@refresh');
	    
	    // Below mention routes are available only for the authenticated users.
	    Route::middleware('auth:api')->group(function () {
	        // Get user info
	        Route::get('user', 'AuthController@user');
	        // Logout user from application
	        Route::post('logout', 'AuthController@logout');

            // auth user
            Route::resource('apiprofile', 'UserController', ['except' => ['index', 'show']]);
            Route::post('apiubahpassword', 'UserController@ubah_password');
            Route::put('apiusertour/{id}', 'UserController@update_usertour');

            // penyakit user
            Route::post('apipenyakit', 'PenyakitController@store');
            Route::put('apipenyakit/{id}', 'PenyakitController@update_penyakit');
            Route::patch('apipenyakit/{id}', 'PenyakitController@update');

            // penyakit user
            Route::post('apiobat', 'ObatController@store');
            Route::put('apiobat/{id}', 'ObatController@update_obat');
            Route::patch('apiobat/{id}', 'ObatController@update');

            // alamat user
            Route::resource('apialamat', 'UserAlamatController', ['except' => ['index', 'show']]);
            Route::put('apialamatstatus/{id}', 'UserAlamatController@update_status');
            Route::get('apialamatcek', 'UserAlamatController@alamat_cek');

            // auth bayi
            Route::resource('apibayi', 'BayiController', ['except' => ['show']]);
            Route::put('apibayiremove/{id}/{status}', 'BayiController@remove');
            Route::get('apibayilist', 'BayiController@bayi_list');


            // donasi bayi
            Route::get('apidonasiorder', 'DonasiController@donasi_order');
            Route::post('apidonasiorder', 'DonasiController@donasi_order_store');
            Route::put('apidonasiorder/{id}', 'DonasiController@status_order');
            Route::resource('apidonasi', 'DonasiController', ['except' => ['show']]);
            Route::put('apidonasiremove/{id}/{status}', 'DonasiController@remove');
            Route::get('apidonasiedit/{id}', 'DonasiController@show_edit_donasi');

            Route::get('apidonasirequest', 'DonasiController@donasi_request');
            Route::delete('apidonasirequest/{id}', 'DonasiController@donasi_request_reject');
            Route::put('apidonasirequest/{id}', 'DonasiController@donasi_request_approve');
            Route::put('apidonasisend/{id}', 'DonasiController@donasi_request_send');

            Route::get('apicart', 'DonasiController@donasi_cart');
            Route::get('apicheckout', 'DonasiController@donasi_checkout');
            Route::put('apicheckout', 'DonasiController@donasi_checkout_update');
            Route::put('apistockupdate/{id}', 'DonasiController@stock_update');
            Route::delete('apistockupdate/{id}', 'DonasiController@stock_delete');

            // kategori info & polling
            Route::resource('apikateogori', 'KategoriController', ['except' => ['index', 'show']]);
            // info
            Route::resource('apiinfo', 'InfoController', ['except' => ['show']]);
            Route::put('apiinforemove/{id}/{status}', 'InfoController@remove');
            Route::get('apiinfoedit/{id}', 'InfoController@show_edit_info');

            Route::post('apiinfocomment', 'InfoController@store_info_comment');
            // polling
            Route::resource('apipolling', 'PollingController', ['except' => ['index', 'show']]);
            Route::put('apipollingremove/{id}/{status}', 'PollingController@remove');
            Route::post('apipollinguser', 'PollingController@polling_user');

            // logistic
            Route::get('apilogisticuser', 'LogisticController@user_logistic');
            Route::put('apilogisticuser/{id}', 'LogisticController@user_logistic_update');

            Route::get('apisilsilah/{jenis}', 'SilsilahController@get_silsilah');



            

	    });
    });
});
