<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/



Route::group(['prefix' => '/'], function () {
	Route::get('/clear-cache', function() {
	    Artisan::call('cache:clear');
	    Artisan::call('route:clear');
	    Artisan::call('view:clear ');
	    Artisan::call('config:clear');
	    return "Cache is cleared";
	});

	Route::get('/{name?}/{id?}', ['as' => 'homePage', 'uses' => 'WebController@index']);

});